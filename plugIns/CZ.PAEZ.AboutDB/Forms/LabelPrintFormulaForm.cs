﻿using Kingdee.BOS.App.Data;
using Kingdee.BOS.Core.Bill.PlugIn;
using Kingdee.BOS.Core.DynamicForm;
using Kingdee.BOS.Core.DynamicForm.PlugIn.Args;
using Kingdee.BOS.Orm.DataEntity;
using Kingdee.BOS.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace CZ.PAEZ.AboutDB.Forms
{
    [Description("标签打印模板适配公式")]
    [HotUpdate]
    public class LabelPrintFormulaForm : AbstractBillPlugIn
    {
        public override void AfterBarItemClick(AfterBarItemClickEventArgs e)
        {
            base.AfterBarItemClick(e);
            string key = e.BarItemKey.ToUpperInvariant();
            switch (key)
            {
                case "TBREFRESH":  //tbRefresh  刷新物料
                    Act_ABIC_RefreshPrintTempTypeOfMaterial();
                    break;
            }
        }

        public override void BeforeDoOperation(BeforeDoOperationEventArgs e)
        {
            base.BeforeDoOperation(e);
            string opKey = e.Operation.FormOperation.Operation.ToUpperInvariant();
            if (opKey.Equals("SAVE"))
            {
                Act_BDO_VerifyRepeatedRow(e);
            }
        }

        /// <summary>
        /// 保存前检验是否存在重复的规则
        /// </summary>
        /// <param name="e"></param>
        private void Act_BDO_VerifyRepeatedRow(BeforeDoOperationEventArgs e)
        {
            var entity = this.Model.DataObject["FEntity"] as DynamicObjectCollection;
            for (int i = 0; i < entity.Count - 1; i++)
            {
                for (int j = i + 1; j < entity.Count; j++)
                {
                    if(entity[i]["F_PAEZ_DBJYBZ1"].Equals(entity[j]["F_PAEZ_DBJYBZ1"])
                       && entity[i]["F_CZ_SCLB"].Equals(entity[j]["F_CZ_SCLB"])
                       && entity[i]["FMtlNoPrefix"].Equals(entity[j]["FMtlNoPrefix"]))
                    {
                        this.View.ShowErrMessage($"第{i + 1}行和第{j + 1}行的条件重复，不允许保存！");
                        e.Cancel = true;
                    }
                }
            }
        }


        /// <summary>
        /// 刷新物料的标签打印模板类型
        /// </summary>
        private void Act_ABIC_RefreshPrintTempTypeOfMaterial()
        {
            this.View.ShowMessage("此操作将修改所有物料对应的打印模板类型，需谨慎操作，确定要继续进行吗？", MessageBoxOptions.YesNo, (result) =>
            {
                if(result == MessageBoxResult.Yes)
                {
                    string sql = "exec proc_czly_UpdateMtlRealPrintLabel";
                    DBUtils.Execute(this.Context, sql);
                    this.View.ShowMessage("执行完成。");
                }
            });
        }



    }
}
