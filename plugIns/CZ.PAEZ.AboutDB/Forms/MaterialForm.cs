using Kingdee.BOS.App.Data;
using Kingdee.BOS.Core.Bill.PlugIn;
using Kingdee.BOS.Core.DynamicForm.PlugIn.Args;
using Kingdee.BOS.Orm.DataEntity;
using Kingdee.BOS.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;


namespace CZ.PAEZ.AboutDB.Forms
{
    [Description("物料插件")]
    [HotUpdate]
    public class MaterialForm : AbstractBillPlugIn
    {
        public override void AfterBarItemClick(AfterBarItemClickEventArgs e)
        {
            base.AfterBarItemClick(e);
            string key = e.BarItemKey.ToUpperInvariant();
            switch (key)
            {
                case "PAEZ_TBUPDATEPRINTLABEL": //PAEZ_tbUpdatePrintLabel 更新实际打标标志
                    Act_ABIC_UpdateRealPrintLabel();
                    break;
            }
        }

        /// <summary>
        /// 更新实际打标标志
        /// </summary>
        private void Act_ABIC_UpdateRealPrintLabel()
        {
            string status = this.Model.GetValue("FDocumentStatus").ToString();
            if (status == "Z") return;

            string mid = this.Model.DataObject["Id"].ToString();
            string sql = $"exec proc_czly_UpdateMtlRealPrintLabel @mid={mid}";
            DBUtils.Execute(Context, sql);
            this.View.Refresh();
        }
    }
}
