﻿using Kingdee.BOS.App.Data;
using Kingdee.BOS.Core.Bill.PlugIn;
using Kingdee.BOS.Core.DynamicForm.PlugIn.Args;
using Kingdee.BOS.Orm.DataEntity;
using Kingdee.BOS.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace CZ.PAEZ.AboutDB.Forms
{
    [Description("UDI最大码")]
    [HotUpdate]
    public class UDIMaxIDForm : AbstractBillPlugIn
    {

        public override void AfterBarItemClick(AfterBarItemClickEventArgs e)
        {
            base.AfterBarItemClick(e);
            string key = e.BarItemKey.ToUpperInvariant();
            switch (key)
            {
                case "PAEZ_TBMAX":
                    Act_BDO_SetMaxUDI();
                    break;
            }
        }

        private void Act_BDO_SetMaxUDI()
        {
            var FUseOrg = this.Model.GetValue("FUseOrgID");
            if(FUseOrg == null)
            {
                return;
            }
            string orgId = (FUseOrg as DynamicObject)["Id"].ToString();
            string sql = $"SELECT MAX(CONVERT(INT, F_CZ_UDI)) FMaxUDI FROM T_BD_MATERIAL WHERE FUseOrgID={orgId}";
            var udi = DBUtils.ExecuteDynamicObject(Context, sql).FirstOrDefault();
            long max = 100000;
            if(udi != null)
            {
                max = Convert.ToInt64(udi["FMaxUDI"]);
            }
            this.Model.SetValue("FCurrMaxUDI", max);
        }
    }
}
