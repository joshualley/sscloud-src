﻿using Kingdee.BOS.App.Data;
using Kingdee.BOS.Core.Bill.PlugIn;
using Kingdee.BOS.Core.DynamicForm.PlugIn.Args;
using Kingdee.BOS.Orm.DataEntity;
using Kingdee.BOS.ServiceHelper;
using Kingdee.BOS.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace CZ.PAEZ.AboutDB.Forms
{
    [Description("物料数据修改审批表单")]
    [HotUpdate]
    public class MtlModifyForm : AbstractBillPlugIn
    {
        public override void DataChanged(DataChangedEventArgs e)
        {
            base.DataChanged(e);
            string key = e.Field.Key.ToUpperInvariant();
            
            switch (key)
            {
                case "FNUMBER":  // 物料编码  FNUMBER
                    Act_DB_CarryOutMtlPropeties(e);
                    break;
            }
        }


        public override void AfterEntryBarItemClick(AfterBarItemClickEventArgs e)
        {
            base.AfterEntryBarItemClick(e);
            string key = e.BarItemKey.ToUpperInvariant();
            switch (key)
            {
                case "PAEZ_TBGENEUDI": // PAEZ_tbGeneUDI 生成UDI码
                    Act_AEBIC_GeneUDI();
                    break;
            }
        }


        /// <summary>
        /// 生成UDI
        /// </summary>
        private void Act_AEBIC_GeneUDI()
        {
            string status = this.Model.GetValue("FDocumentStatus").ToString();
            if (status == "Z")
            {
                this.View.ShowMessage("请保存后再进行操作!");
                return;
            }
            string FBillTypeID = (this.Model.GetValue("FBillTypeID") as DynamicObject)?["Id"].ToString();
            if (!FBillTypeID.Equals("5fd824115206a7")) // 产品注册数据审批单
            {
                this.View.ShowWarnningMessage("此类型变更单不需要生成UDI!");
                return;
            }
            var entity = this.Model.DataObject["FEntity"] as DynamicObjectCollection;
            var list = new List<int>();
            string info = string.Empty;
            for (int i = 0; i < entity.Count; i++)
            {
                string materialId = (entity[i]["FNUMBER"] as DynamicObject)?["Id"].ToString();
                if (materialId.IsNullOrEmptyOrWhiteSpace() || !entity[i]["F_CZ_UDI"].IsNullOrEmptyOrWhiteSpace())
                {
                    info += $"第{i + 1}行无需生成UDI, 已跳过! \n";
                    continue;
                }
                string sql = string.Format(@"select F_PAEZ_POOrgID from T_BD_Material where FMaterialID={0}", materialId);
                var poOrgId = DBUtils.ExecuteDynamicObject(Context, sql);
                if(poOrgId[0]["F_PAEZ_POOrgID"].ToString() == "0")
                {
                    info += $"第{i + 1} 行物料的需求组织不存在! \n";
                    continue;
                }
                sql = string.Format(@"/*dialect*/
update PAEZ_BD_UDIMaxID set FCurrMaxUDI=FCurrMaxUDI+1 where FUseOrgID={0};
select FCurrMaxUDI FMax from PAEZ_BD_UDIMaxID where FUseOrgID={0}", poOrgId[0]["F_PAEZ_POOrgID"]);
                var maxId = DBUtils.ExecuteDynamicObject(Context, sql);
                if(maxId.Count <= 0)
                    continue;
                // udi为6位，不足位数进行补零
                this.Model.SetValue("F_CZ_UDI", maxId[0]["FMax"].ToString().PadLeft(6, '0'), i);
            }
            BusinessDataServiceHelper.Save(
                this.Context,
                this.View.BillBusinessInfo,
                this.View.Model.DataObject
            );
            info = info == "" ? "生成完毕。" : info;
            this.View.ShowMessage(info);
        }


        /// <summary>
        /// 携带物料属性
        /// </summary>
        private void Act_DB_CarryOutMtlPropeties(DataChangedEventArgs e)
        {
            string FMaterialId = this.Model.GetValue("FNUMBER", e.Row) == null ? "0" :
                (this.Model.GetValue("FNUMBER", e.Row) as DynamicObject)["Id"].ToString();
            if(FMaterialId.Equals("0"))
            {
                return;
            }
            
            string sql = string.Format(@"select FNAME, FSPECIFICATION
    , F_PAEZ_materialCDHPP, F_PAEZ_SQDJHLB, F_PAEZ_FMATERIALGLLB
    , F_PAEZ_FMATERIALZZFS, F_PAEZ_NXQQTY, F_PAEZ_TCLB, F_PAEZ_CBZQ
    , F_PAEZ_DBJYBZ1, F_PAEZ_JYJD, F_PAEZ_CPJSYQBH, F_PAEZ_DQDate
    , F_PAEZ_SMSBH, F_PAEZ_SYNX, F_PAEZ_HZDATE, F_PAEZ_Y4MJH
    , F_CZ_CPFL, F_CZ_XSLB, F_CZ_SCLB, F_CZ_ZYLB, F_CZ_DRCNO
    , F_CZ_UDI, F_CZ_ZYLB, F_CZ_FmateralNCL
    , FPLANSAFESTOCKQTY, FMINPOQTY, FINCREASEQTY, FFIXLEADTIME
    , FDEFAULTVENDORID
from T_BD_MATERIAL m
inner join T_BD_MATERIAL_L ml on m.FMaterialID=ml.FMaterialID
inner join t_BD_MaterialPlan p on m.FMaterialID=p.FMaterialID
inner join t_bd_MaterialPurchase pc on m.FMaterialID=pc.FMaterialID
where m.FMaterialId={0}", FMaterialId);
            var items = DBUtils.ExecuteDynamicObject(this.Context, sql);
            if(items.Count <= 0)
            {
                return;
            }

            // 根据单据类型，携带不同的数据
            string FBillTypeID = (this.Model.GetValue("FBillTypeID") as DynamicObject)?["Id"].ToString();
            switch (FBillTypeID)
            {
                case "5fd823a052053c": // 材料数据审批单
                    this.Model.SetValue("F_CZ_CLFL", items[0]["F_CZ_CPFL"], e.Row); //品种码
                    this.Model.SetValue("F_PAEZ_materialCDHPP", items[0]["F_PAEZ_materialCDHPP"], e.Row); //产地或品牌
                    this.Model.SetValue("F_PAEZ_SQDJHLB", items[0]["F_PAEZ_SQDJHLB"], e.Row); //计划种类
                    this.Model.SetValue("FPLANSAFESTOCKQTY", items[0]["FPLANSAFESTOCKQTY"], e.Row); //储备标准
                    this.Model.SetValue("FMINPOQTY", items[0]["FMINPOQTY"], e.Row); //采购批量
                    this.Model.SetValue("FDEFAULTVENDORID", items[0]["FDEFAULTVENDORID"], e.Row); //供应商
                    break;
                case "5fd823bc520595": // 产品零部件目录数据变更审批单
                    this.Model.SetValue("FFNAME", items[0]["FNAME"], e.Row); 
                    this.Model.SetValue("FFNAME2", items[0]["FNAME"], e.Row); // 现物料名称
                    this.Model.SetValue("FSPECIFICATION", items[0]["FSPECIFICATION"], e.Row); //现规格型号
                    this.Model.SetValue("F_PAEZ_FFMATERIALGLLB", items[0]["F_PAEZ_FMATERIALGLLB"], e.Row); // 管理类别
                    this.Model.SetValue("F_PAEZ_FMATERIALZZFS", items[0]["F_PAEZ_FMATERIALZZFS"], e.Row); // 制造方式
                    break;
                case "5fd823de5205ed": // 产品试样数据登录审批单
                    this.Model.SetValue("F_CZ_XSLB", items[0]["F_CZ_XSLB"], e.Row); //销售类别
                    this.Model.SetValue("F_CZ_SCLB", items[0]["F_CZ_SCLB"], e.Row); //生产类别
                    this.Model.SetValue("F_PAEZ_FFMATERIALGLLB", items[0]["F_PAEZ_FMATERIALGLLB"], e.Row); //管理类别
                    this.Model.SetValue("F_PAEZ_NXQQTY", items[0]["F_PAEZ_NXQQTY"], e.Row);//年需求量
                    this.Model.SetValue("FINCREASEQTY", items[0]["FINCREASEQTY"], e.Row); //最小包装量
                    this.Model.SetValue("F_CZ_ZYLB", items[0]["F_CZ_ZYLB"], e.Row); //作业类别
                    this.Model.SetValue("F_PAEZ_TCLB", items[0]["F_PAEZ_TCLB"], e.Row); //投产模式
                    this.Model.SetValue("FFIXLEADTIME1", items[0]["FFIXLEADTIME"], e.Row); //生产周期（月）
                    this.Model.SetValue("F_PAEZ_CBZQ", items[0]["F_PAEZ_CBZQ"], e.Row); //储备周期
                    this.Model.SetValue("F_PAEZ_DBJYBZ1", items[0]["F_PAEZ_DBJYBZ1"], e.Row); //打标检验标志
                    this.Model.SetValue("F_PAEZ_JYJD", items[0]["F_PAEZ_JYJD"], e.Row); //校验节点
                    this.Model.SetValue("FDEFAULTVENDORID", items[0]["FDEFAULTVENDORID"], e.Row); //供应商
                    break;
                case "5fd823ff52064d": // 产品销售类别修改通知
                    this.Model.SetValue("F_CZ_XSLB", items[0]["F_CZ_XSLB"], e.Row); //销售类别
                    break;
                case "5fd824115206a7": // 产品注册数据审批单
                    this.Model.SetValue("F_PAEZ_CPJSYQBH", items[0]["F_PAEZ_CPJSYQBH"], e.Row); // 产品技术要求编号
                    this.Model.SetValue("F_CZ_DRCNO", items[0]["F_CZ_DRCNO"], e.Row); // 产品注册证号/备案凭证编号
                    if(Convert.ToDateTime(items[0]["F_PAEZ_DQDate"]).Year > 1900)
                    {
                        this.Model.SetValue("F_PAEZ_DQDate", items[0]["F_PAEZ_DQDate"], e.Row); //到期日
                    }
                    this.Model.SetValue("F_PAEZ_SMSBH", items[0]["F_PAEZ_SMSBH"], e.Row); //说明书编号
                    this.Model.SetValue("F_PAEZ_SYNX", items[0]["F_PAEZ_SYNX"], e.Row); //使用年限
                    this.Model.SetValue("F_CZ_UDI", items[0]["F_CZ_UDI"], e.Row); //UDI
                    if (Convert.ToDateTime(items[0]["F_PAEZ_HZDATE"]).Year > 1900)
                    {
                        this.Model.SetValue("F_PAEZ_HZDATE", items[0]["F_PAEZ_HZDATE"], e.Row); //获证日期
                    }
                    break;
                case "5fd82439520751": // 零部件作业数据登录审批单
                    this.Model.SetValue("F_PAEZ_FFMATERIALGLLB", items[0]["F_PAEZ_FMATERIALGLLB"], e.Row); //管理类别
                    this.Model.SetValue("F_PAEZ_FMATERIALZZFS", items[0]["F_PAEZ_FMATERIALZZFS"], e.Row); //制造方式
                    this.Model.SetValue("F_CZ_SCLB", items[0]["F_CZ_SCLB"], e.Row); //生产类别
                    this.Model.SetValue("F_CZ_ZYLB", items[0]["F_CZ_ZYLB"], e.Row); //作业类别
                    this.Model.SetValue("F_PAEZ_NXQQTY", items[0]["F_PAEZ_NXQQTY"], e.Row); //年需求量
                    this.Model.SetValue("FINCREASEQTY", items[0]["FINCREASEQTY"], e.Row); //最小包装量
                    this.Model.SetValue("F_PAEZ_TCLB", items[0]["F_PAEZ_TCLB"], e.Row); //投产模式
                    this.Model.SetValue("F_PAEZ_JYJD", items[0]["F_PAEZ_JYJD"], e.Row); //校验节点
                    this.Model.SetValue("FDEFAULTVENDORID", items[0]["FDEFAULTVENDORID"], e.Row); //供应商
                    break;
                case "5fd824505207a3": // 毛坯数据登录审批单
                    this.Model.SetValue("F_CZ_SCLB", items[0]["F_CZ_SCLB"], e.Row); //生产类别
                    this.Model.SetValue("F_CZ_ZYLB", items[0]["F_CZ_ZYLB"], e.Row); //作业类别
                    this.Model.SetValue("FFIXLEADTIME1", items[0]["FFIXLEADTIME"], e.Row); //生产周期（月）
                    this.Model.SetValue("F_PAEZ_Y4MJH", items[0]["F_PAEZ_Y4MJH"], e.Row); //Y4模具号
                    this.Model.SetValue("FDEFAULTVENDORID", items[0]["FDEFAULTVENDORID"], e.Row); //供应商
                    break;
                case "5fd823135204ce": // 变更产品目录数据审批单
                    this.Model.SetValue("FFNAME", items[0]["FNAME"], e.Row); 
                    this.Model.SetValue("FFNAME2", items[0]["FNAME"], e.Row); //现物料名称
                    this.Model.SetValue("FSPECIFICATION", items[0]["FSPECIFICATION"], e.Row); //现规格型号
                    this.Model.SetValue("F_CZ_CPFL", items[0]["F_CZ_CPFL"], e.Row); //现分类码
                    break;
                case "5fd824265206ff": // 产品作业数据登录审批单
                    this.Model.SetValue("F_CZ_XSLB", items[0]["F_CZ_XSLB"], e.Row); //销售类别
                    this.Model.SetValue("F_CZ_SCLB", items[0]["F_CZ_SCLB"], e.Row); //生产类别
                    this.Model.SetValue("F_PAEZ_NXQQTY", items[0]["F_PAEZ_NXQQTY"], e.Row); //年需求量
                    this.Model.SetValue("FINCREASEQTY", items[0]["FINCREASEQTY"], e.Row); //最小包装量
                    this.Model.SetValue("F_CZ_ZYLB", items[0]["F_CZ_ZYLB"], e.Row); //作业类别
                    this.Model.SetValue("F_CZ_FmateralNCL", items[0]["F_CZ_FmateralNCL"], e.Row); //年产量
                    this.Model.SetValue("FFIXLEADTIME1", items[0]["FFIXLEADTIME"], e.Row); //生产周期（月）
                    this.Model.SetValue("F_PAEZ_CBZQ", items[0]["F_PAEZ_CBZQ"], e.Row); //储备周期
                    this.Model.SetValue("F_PAEZ_TCLB", items[0]["F_PAEZ_TCLB"], e.Row); //投产模式
                    this.Model.SetValue("F_PAEZ_DBJYBZ1", items[0]["F_PAEZ_DBJYBZ1"], e.Row); //打标检验标志
                    this.Model.SetValue("F_PAEZ_JYJD", items[0]["F_PAEZ_JYJD"], e.Row); //校验节点
                    this.Model.SetValue("FDEFAULTVENDORID", items[0]["FDEFAULTVENDORID"], e.Row); //供应商
                    break;
            }

            
        }



    }
}
