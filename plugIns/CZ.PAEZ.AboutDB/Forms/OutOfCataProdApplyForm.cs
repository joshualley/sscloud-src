﻿using Kingdee.BOS.App.Data;
using Kingdee.BOS.Core.Bill.PlugIn;
using Kingdee.BOS.Core.DynamicForm.PlugIn.Args;
using Kingdee.BOS.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace CZ.PAEZ.AboutDB.Forms
{
    [HotUpdate]
    [Description("目录外产品加工申请单")]
    public class OutOfCataProdApplyForm : AbstractBillPlugIn
    {
        public override void AfterBarItemClick(AfterBarItemClickEventArgs e)
        {
            base.AfterBarItemClick(e);
            string key = e.BarItemKey.ToUpperInvariant();
            switch (key)
            {
                case "PAEZ_TBGENECUSTNO": //PAEZ_tbGeneCustNo 获取定制件编码
                    Act_ABIC_GetGeneMtlNumber();
                    break;
            }
            
        }

        /// <summary>
        /// 获取生成的定制件编码、批号
        /// </summary>
        private void Act_ABIC_GetGeneMtlNumber()
        {
            string F_PAEZ_MLWCPBMa = this.Model.GetValue("F_PAEZ_MLWCPBMa")?.ToString();
            string F_PAEZ_SCPHa = this.Model.GetValue("F_PAEZ_SCPHa")?.ToString();
            if (!F_PAEZ_MLWCPBMa.IsNullOrEmptyOrWhiteSpace() && !F_PAEZ_SCPHa.IsNullOrEmptyOrWhiteSpace()) 
            {
                this.View.ShowErrMessage("已经获取成功，请勿重复获取！");
                return;
            }

            string sql = "/*dialect*/select FNumber, FLot from dbo.func_czly_GeneCustomizedMtlNumber()";
            var obj = DBUtils.ExecuteDynamicObject(Context, sql).FirstOrDefault();
            if (obj == null) 
            {
                this.View.ShowMessage("获取失败！");
                return;
            }

            this.Model.SetValue("F_PAEZ_MLWCPBMa", obj["FNumber"]);
            this.Model.SetValue("F_PAEZ_SCPHa", obj["FLot"]);

            this.View.ShowMessage("获取成功。");
        }
    }
}
