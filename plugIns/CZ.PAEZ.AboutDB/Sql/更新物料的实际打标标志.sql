-- 更新物料实际打标标志
alter proc proc_czly_UpdateMtlRealPrintLabel(
    @fid bigint=-1, -- 物料变更单fid
    @mid bigint=-1  -- 物料ID
) as
begin
set nocount on

declare @t_material table
(
    FMaterialId bigint, 
    FNumber varchar(55), 
    F_PAEZ_DBJYBZ1 varchar(55),
    F_CZ_SCLB varchar(55), 
    FRealPrintLabel varchar(55)
)

if @fid=-1
begin
    if @mid=-1
    begin
        insert into @t_material(FMaterialId, FNumber, F_PAEZ_DBJYBZ1, F_CZ_SCLB, FRealPrintLabel)
        select FMaterialId, FNumber, F_PAEZ_DBJYBZ1, F_CZ_SCLB, F_PAEZ_DBJYBZ1
        from T_BD_MATERIAL
    end
    else
    begin
        insert into @t_material(FMaterialId, FNumber, F_PAEZ_DBJYBZ1, F_CZ_SCLB, FRealPrintLabel)
        select FMaterialId, FNumber, F_PAEZ_DBJYBZ1, F_CZ_SCLB, F_PAEZ_DBJYBZ1
        from T_BD_MATERIAL where FMasterId=(select FMasterId from T_BD_MATERIAL where FMaterialId=@mid)
    end
end
else
begin
    insert into @t_material(FMaterialId, FNumber, F_PAEZ_DBJYBZ1, F_CZ_SCLB, FRealPrintLabel)
    select FMaterialId, FNumber, F_PAEZ_DBJYBZ1, F_CZ_SCLB, F_PAEZ_DBJYBZ1
    from (
        select distinct m.FMasterId from PAEZ_t_Cust_WLSJBGSPDEntry we
        inner join T_BD_Material m on m.FMaterialId=we.FNUMBER
        where we.FID=@fid
    ) mm 
    inner join T_BD_MATERIAL m on m.FMasterId=mm.FMasterId
end

-- 根据打标检验标志获取结果
update t set t.FRealPrintLabel=lpe.FRealPrintLabel from @t_material t
inner join PAEZ_t_LabelPrintEntry lpe on lpe.F_PAEZ_DBJYBZ1=t.F_PAEZ_DBJYBZ1 
and lpe.F_CZ_SCLB='' and lpe.FMtlNoPrefix='' and t.FRealPrintLabel<>lpe.FRealPrintLabel
-- 根据双重条件判断
update t set t.FRealPrintLabel=lpe.FRealPrintLabel from @t_material t
inner join PAEZ_t_LabelPrintEntry lpe on lpe.F_PAEZ_DBJYBZ1=t.F_PAEZ_DBJYBZ1 
and lpe.F_CZ_SCLB=t.F_CZ_SCLB and lpe.FMtlNoPrefix='' and lpe.F_CZ_SCLB<>'' 
and t.FRealPrintLabel<>lpe.FRealPrintLabel
-- 定制件
update t set t.FRealPrintLabel=lpe.FRealPrintLabel from @t_material t
inner join PAEZ_t_LabelPrintEntry lpe on t.FNumber like lpe.FMtlNoPrefix+'%' 
where lpe.F_CZ_SCLB=t.F_CZ_SCLB and lpe.FMtlNoPrefix<>'' and lpe.F_CZ_SCLB<>''
and t.FRealPrintLabel<>lpe.FRealPrintLabel

-- 更新物料
update m set m.F_PAEZ_RealPrintLabel=t.FRealPrintLabel from T_BD_MATERIAL m
inner join @t_material t on m.FMaterialId=t.FMaterialId
where m.F_PAEZ_RealPrintLabel<>t.FRealPrintLabel


end

-- exec dbo.proc_czly_UpdateMtlRealPrintLabel @fid=0, @mid=0