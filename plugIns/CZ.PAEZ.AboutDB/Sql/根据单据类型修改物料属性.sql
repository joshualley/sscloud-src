-- 根据单据类型修改物料的属性
alter proc proc_czly_ModifyMtlProperty(
    @fid int -- 物料变更单内码
) as
begin
-- declare @fid int

declare @type varchar(55)=''
select @type=FBillTypeID from PAEZ_t_WLSJBGSPD where FID=@fid

if @type = '5fd823a052053c' -- 1.材料数据审批单
begin
    update m set 
        m.F_PAEZ_WLDBBS=mm.F_PAEZ_WLDBBS, -- 物料打标标识
        m.F_CZ_CPFL=mm.F_CZ_CLFL, 
        m.F_PAEZ_materialCDHPP=mm.F_PAEZ_materialCDHPP, 
        m.F_PAEZ_SQDJHLB=mm.F_PAEZ_SQDJHLB
    from (
        select m.FMasterId, we.* from PAEZ_t_Cust_WLSJBGSPDEntry we
        inner join T_BD_Material m on m.FMaterialId=we.FNUMBER
        where we.FID=@fid
    ) mm 
    inner join T_BD_Material m on mm.FMasterId=m.FMasterId

    update m set 
        m.FPLANSAFESTOCKQTY=mm.FPLANSAFESTOCKQTY, 
        m.FMINPOQTY=mm.FMINPOQTY 
    from (
        select m.FMasterId, we.* from PAEZ_t_Cust_WLSJBGSPDEntry we
        inner join T_BD_Material m on m.FMaterialId=we.FNUMBER
        where we.FID=@fid
    ) mm 
    inner join T_BD_Material mt on mm.FMasterId=mt.FMasterId
    inner join t_BD_MaterialPlan m on mt.FMaterialId=m.FMaterialId

    update m set m.FDEFAULTVENDORID=mm.FDEFAULTVENDORID
    from (
        select m.FMasterId, we.* from PAEZ_t_Cust_WLSJBGSPDEntry we
        inner join T_BD_Material m on m.FMaterialId=we.FNUMBER
        where we.FID=@fid
    ) mm 
    inner join T_BD_Material mt on mm.FMasterId=mt.FMasterId
    inner join t_bd_MaterialPurchase m on mt.FMaterialId=m.FMaterialId
end
else if @type = '5fd823bc520595' -- 2.产品零部件目录数据变更审批单
begin
    update m set 
        m.FNAME=mm.FFNAME, 
        m.FSPECIFICATION=mm.FSPECIFICATION
    from (
        select m.FMasterId, we.* from PAEZ_t_Cust_WLSJBGSPDEntry we
        inner join T_BD_Material m on m.FMaterialId=we.FNUMBER
        where we.FID=@fid
    ) mm 
    inner join T_BD_Material mt on mm.FMasterId=mt.FMasterId
    inner join T_BD_MATERIAL_L m on mt.FMaterialId=m.FMaterialId

    update m set 
        m.F_PAEZ_WLDBBS=mm.F_PAEZ_WLDBBS, -- 物料打标标识
        m.F_PAEZ_FMATERIALGLLB=mm.F_PAEZ_FFMATERIALGLLB, 
        m.F_PAEZ_FMATERIALZZFS=mm.F_PAEZ_FMATERIALZZFS1 
    from (
        select m.FMasterId, we.* from PAEZ_t_Cust_WLSJBGSPDEntry we
        inner join T_BD_Material m on m.FMaterialId=we.FNUMBER
        where we.FID=@fid
    ) mm 
    inner join T_BD_Material m on mm.FMasterId=m.FMasterId

    -- 生成物料变更记录
    exec dbo.proc_czly_GeneMtlModifyRecord @fid=@fid
end
else if @type = '5fd823de5205ed' -- 3.产品试样数据登录审批单
begin
    update m set 
        m.F_PAEZ_WLDBBS=mm.F_PAEZ_WLDBBS, -- 物料打标标识
        m.F_PAEZ_WLTCMS=mm.F_PAEZ_WLTCMS, -- 物料投产模式
        m.F_CZ_XSLB=mm.F_CZ_XSLB, 
        m.F_CZ_SCLB=mm.F_CZ_SCLB, 
        m.F_PAEZ_FMATERIALGLLB=mm.F_PAEZ_FFMATERIALGLLB,
        m.F_PAEZ_NXQQTY=mm.F_PAEZ_NXQQTY, 
        m.F_CZ_ZYLB=mm.F_CZ_ZYLB, 
        m.F_PAEZ_TCLB=mm.F_PAEZ_TCLB, 
        m.F_PAEZ_CBZQ=mm.F_PAEZ_CBZQ, 
        m.F_PAEZ_DBJYBZ1=mm.F_PAEZ_DBJYBZ1, 
        m.F_PAEZ_JYJD=mm.F_PAEZ_JYJD
    from (
        select m.FMasterId, we.* from PAEZ_t_Cust_WLSJBGSPDEntry we
        inner join T_BD_Material m on m.FMaterialId=we.FNUMBER
        where we.FID=@fid
    ) mm 
    inner join T_BD_MATERIAL m on mm.FMasterId=m.FMasterId

    update m set 
        m.FINCREASEQTY=mm.FINCREASEQTY, 
        m.FFIXLEADTIME=mm.FFIXLEADTIME1
    from (
        select m.FMasterId, we.* from PAEZ_t_Cust_WLSJBGSPDEntry we
        inner join T_BD_Material m on m.FMaterialId=we.FNUMBER
        where we.FID=@fid
    ) mm 
    inner join T_BD_Material mt on mm.FMasterId=mt.FMasterId
    inner join t_BD_MaterialPlan m on mt.FMaterialId=m.FMaterialId

    update m set m.FDEFAULTVENDORID=mm.FDEFAULTVENDORID
    from (
        select m.FMasterId, we.* from PAEZ_t_Cust_WLSJBGSPDEntry we
        inner join T_BD_Material m on m.FMaterialId=we.FNUMBER
        where we.FID=@fid
    ) mm 
    inner join T_BD_Material mt on mm.FMasterId=mt.FMasterId
    inner join t_bd_MaterialPurchase m on mt.FMaterialId=m.FMaterialId
    -- 更新物料实际打标标志
    exec dbo.proc_czly_UpdateMtlRealPrintLabel @fid=@fid
end
else if @type = '5fd823ff52064d' -- 4.产品销售类别修改通知
begin
    update m set 
        m.F_PAEZ_WLDBBS=mm.F_PAEZ_WLDBBS, -- 物料打标标识
        m.F_CZ_XSLB=mm.F_CZ_XSLB
    from (
        select m.FMasterId, we.* from PAEZ_t_Cust_WLSJBGSPDEntry we
        inner join T_BD_Material m on m.FMaterialId=we.FNUMBER
        where we.FID=@fid
    ) mm 
    inner join T_BD_MATERIAL m on mm.FMasterId=m.FMasterId
end
else if @type = '5fd824115206a7' -- 5.产品注册数据审批单
begin
    -- UDI每个组织不同, 单独更新
    update m set 
        m.F_PAEZ_WLDBBS=mm.F_PAEZ_WLDBBS, -- 物料打标标识
        m.F_CZ_UDI=mm.F_CZ_UDI
    from (
        select * from PAEZ_t_Cust_WLSJBGSPDEntry where FID=@fid
    ) mm
    inner join T_BD_MATERIAL m on mm.FNUMBER=m.FMaterialId

    update m set
        m.F_PAEZ_CPJSYQBH=mm.F_PAEZ_CPJSYQBH, m.F_CZ_DRCNO=mm.F_CZ_DRCNO, 
        m.F_PAEZ_PrdLicense=dbo.proc_zjfj_GetZch(mm.F_CZ_DRCNO), 
        m.F_CZ_YLLB=case 
            when isnull(mm.F_CZ_DRCNO, '')='' then '5f76bff3c718e6' -- 民用
            when dbo.proc_zjfj_GetZch(mm.F_CZ_DRCNO)='' then '5f76c010c718e8' -- I类 (含有备字的)
            when right(left(dbo.proc_zjfj_GetZch(mm.F_CZ_DRCNO), 5), 1)='1' then '5f76c010c718e8' -- I类
            when right(left(dbo.proc_zjfj_GetZch(mm.F_CZ_DRCNO), 5), 1)='2' then '5f76c01fc718ea' -- II类
            when right(left(dbo.proc_zjfj_GetZch(mm.F_CZ_DRCNO), 5), 1)='3' then '5f76c02bc718ec' -- III类
            when right(left(dbo.proc_zjfj_GetZch(mm.F_CZ_DRCNO), 5), 1)='4' then '5f76c036c718ee' -- IV类
            end, 
        m.F_PAEZ_DQDate=mm.F_PAEZ_DQDate, m.F_PAEZ_SMSBH=mm.F_PAEZ_SMSBH, 
        m.F_PAEZ_SYNX=mm.F_PAEZ_SYNX, m.F_PAEZ_HZDATE=mm.F_PAEZ_HZDATE
    from (
        select m.FMasterId, we.* from PAEZ_t_Cust_WLSJBGSPDEntry we
        inner join T_BD_Material m on m.FMaterialId=we.FNUMBER
        where we.FID=@fid
    ) mm 
    inner join T_BD_MATERIAL m on mm.FMasterId=m.FMasterId

    -- 生成物料变更记录
    exec dbo.proc_czly_GeneMtlModifyRecord @fid=@fid
end
else if @type = '5fd82439520751' -- 6.零部件作业数据登录审批单
begin
    update m set 
        m.F_PAEZ_WLDBBS=mm.F_PAEZ_WLDBBS, -- 物料打标标识
        m.F_PAEZ_WLTCMS=mm.F_PAEZ_WLTCMS, -- 物料投产模式
        m.F_PAEZ_FMATERIALGLLB=mm.F_PAEZ_FFMATERIALGLLB, 
        m.F_PAEZ_FMATERIALZZFS=mm.F_PAEZ_FMATERIALZZFS1, 
        m.F_CZ_SCLB=mm.F_CZ_SCLB, 
        m.F_CZ_ZYLB=mm.F_CZ_ZYLB, 
        m.F_PAEZ_NXQQTY=mm.F_PAEZ_NXQQTY, 
        m.F_PAEZ_TCLB=mm.F_PAEZ_TCLB, 
        m.F_PAEZ_JYJD=mm.F_PAEZ_JYJD
    from (
        select m.FMasterId, we.* from PAEZ_t_Cust_WLSJBGSPDEntry we
        inner join T_BD_Material m on m.FMaterialId=we.FNUMBER
        where we.FID=@fid
    ) mm 
    inner join T_BD_MATERIAL m on mm.FMasterId=m.FMasterId

    update m set m.FINCREASEQTY=mm.FINCREASEQTY
    from (
        select m.FMasterId, we.* from PAEZ_t_Cust_WLSJBGSPDEntry we
        inner join T_BD_Material m on m.FMaterialId=we.FNUMBER
        where we.FID=@fid
    ) mm 
    inner join T_BD_Material mt on mm.FMasterId=mt.FMasterId
    inner join t_BD_MaterialPlan m on mt.FMaterialId=m.FMaterialId

    update m set m.FDEFAULTVENDORID=mm.FDEFAULTVENDORID
    from (
        select m.FMasterId, we.* from PAEZ_t_Cust_WLSJBGSPDEntry we
        inner join T_BD_Material m on m.FMaterialId=we.FNUMBER
        where we.FID=@fid
    ) mm 
    inner join T_BD_Material mt on mm.FMasterId=mt.FMasterId
    inner join t_bd_MaterialPurchase m on mt.FMaterialId=m.FMaterialId
end
else if @type = '5fd824505207a3' -- 7.毛坯数据登录审批单
begin
    update m set 
        m.F_PAEZ_WLDBBS=mm.F_PAEZ_WLDBBS, -- 物料打标标识
        m.F_PAEZ_WLTCMS=mm.F_PAEZ_WLTCMS, -- 物料投产模式
        m.F_CZ_SCLB=mm.F_CZ_SCLB, 
        m.F_CZ_ZYLB=mm.F_CZ_ZYLB, 
        m.F_PAEZ_Y4MJH=mm.F_PAEZ_Y4MJH
    from (
        select m.FMasterId, we.* from PAEZ_t_Cust_WLSJBGSPDEntry we
        inner join T_BD_Material m on m.FMaterialId=we.FNUMBER
        where we.FID=@fid
    ) mm 
    inner join T_BD_MATERIAL m on mm.FMasterId=m.FMasterId

    update m set m.FFIXLEADTIME=mm.FFIXLEADTIME1
    from (
        select m.FMasterId, we.* from PAEZ_t_Cust_WLSJBGSPDEntry we
        inner join T_BD_Material m on m.FMaterialId=we.FNUMBER
        where we.FID=@fid
    ) mm 
    inner join T_BD_Material mt on mm.FMasterId=mt.FMasterId
    inner join t_BD_MaterialPlan m on mt.FMaterialId=m.FMaterialId

    update m set m.FDEFAULTVENDORID=mm.FDEFAULTVENDORID
    from (
        select m.FMasterId, we.* from PAEZ_t_Cust_WLSJBGSPDEntry we
        inner join T_BD_Material m on m.FMaterialId=we.FNUMBER
        where we.FID=@fid
    ) mm 
    inner join T_BD_Material mt on mm.FMasterId=mt.FMasterId
    inner join t_bd_MaterialPurchase m on mt.FMaterialId=m.FMaterialId
end
else if @type = '5fd823135204ce' -- 8.变更产品目录数据审批单
begin
    update m set 
        m.FNAME=mm.FFNAME, 
        m.FSPECIFICATION=mm.FSPECIFICATION
    from (
        select m.FMasterId, we.* from PAEZ_t_Cust_WLSJBGSPDEntry we
        inner join T_BD_Material m on m.FMaterialId=we.FNUMBER
        where we.FID=@fid
    ) mm 
    inner join T_BD_Material mt on mm.FMasterId=mt.FMasterId
    inner join T_BD_MATERIAL_L m on mt.FMaterialId=m.FMaterialId

    update m set m.F_CZ_CPFL=mm.F_CZ_CPFL
    from (
        select m.FMasterId, we.* from PAEZ_t_Cust_WLSJBGSPDEntry we
        inner join T_BD_Material m on m.FMaterialId=we.FNUMBER
        where we.FID=@fid
    ) mm 
    inner join T_BD_MATERIAL m on mm.FMasterId=m.FMasterId
    -- 生成物料变更记录
    exec dbo.proc_czly_GeneMtlModifyRecord @fid=@fid
end
else if @type = '5fd824265206ff' -- 9.产品作业数据登录审批单
begin
    update m set 
        m.F_PAEZ_WLDBBS=mm.F_PAEZ_WLDBBS, -- 物料打标标识
        m.F_PAEZ_WLTCMS=mm.F_PAEZ_WLTCMS, -- 物料投产模式
        m.F_CZ_XSLB=mm.F_CZ_XSLB, 
        m.F_PAEZ_NXQQTY=mm.F_PAEZ_NXQQTY, 
        m.F_CZ_ZYLB=mm.F_CZ_ZYLB, 
        m.F_CZ_FmateralNCL=mm.F_CZ_FmateralNCL, 
        m.F_PAEZ_CBZQ=mm.F_PAEZ_CBZQ, 
        m.F_PAEZ_TCLB=mm.F_PAEZ_TCLB, 
        m.F_PAEZ_DBJYBZ1=mm.F_PAEZ_DBJYBZ1, 
        m.F_PAEZ_JYJD=mm.F_PAEZ_JYJD
    from (
        select m.FMasterId, we.* from PAEZ_t_Cust_WLSJBGSPDEntry we
        inner join T_BD_Material m on m.FMaterialId=we.FNUMBER
        where we.FID=@fid
    ) mm 
    inner join T_BD_MATERIAL m on mm.FMasterId=m.FMasterId

    update m set 
        m.FINCREASEQTY=mm.FINCREASEQTY, 
        m.FFIXLEADTIME=mm.FFIXLEADTIME1
    from (
        select m.FMasterId, we.* from PAEZ_t_Cust_WLSJBGSPDEntry we
        inner join T_BD_Material m on m.FMaterialId=we.FNUMBER
        where we.FID=@fid
    ) mm 
    inner join T_BD_Material mt on mm.FMasterId=mt.FMasterId
    inner join t_BD_MaterialPlan m on mt.FMaterialId=m.FMaterialId

    update m set m.FDEFAULTVENDORID=mm.FDEFAULTVENDORID
    from (
        select m.FMasterId, we.* from PAEZ_t_Cust_WLSJBGSPDEntry we
        inner join T_BD_Material m on m.FMaterialId=we.FNUMBER
        where we.FID=@fid
    ) mm 
    inner join T_BD_Material mt on mm.FMasterId=mt.FMasterId
    inner join t_bd_MaterialPurchase m on mt.FMaterialId=m.FMaterialId
    -- 更新物料实际打标标志
    exec dbo.proc_czly_UpdateMtlRealPrintLabel @fid=@fid
end

end

-- exec proc_czly_ModifyMtlProperty @fid=0