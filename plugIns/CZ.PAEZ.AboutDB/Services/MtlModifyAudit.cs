﻿using Kingdee.BOS.App.Data;
using Kingdee.BOS.Core.DynamicForm.PlugIn;
using Kingdee.BOS.Core.DynamicForm.PlugIn.Args;
using Kingdee.BOS.Orm.DataEntity;
using Kingdee.BOS.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace CZ.PAEZ.AboutDB.Services
{
    [Description("物料数据修改审批单之审核服务")]
    [HotUpdate]
    public class MtlModifyAudit : AbstractOperationServicePlugIn
    {
        public override void EndOperationTransaction(EndOperationTransactionArgs e)
        {
            base.EndOperationTransaction(e);
            foreach(var dataEntity in e.DataEntitys)
            {
                string fid = dataEntity["Id"].ToString();
                string sql = $"exec proc_czly_ModifyMtlProperty @fid={fid}";
                DBUtils.Execute(Context, sql);
            }
        }
    }
}
