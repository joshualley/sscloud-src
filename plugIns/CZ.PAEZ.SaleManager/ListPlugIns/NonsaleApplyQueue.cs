﻿using CZ.PAEZ.SaleManager.Util;
using Kingdee.BOS.App.Data;
using Kingdee.BOS.Core.DynamicForm.PlugIn.Args;
using Kingdee.BOS.Core.List.PlugIn;
using Kingdee.BOS.Core.List.PlugIn.Args;
using Kingdee.BOS.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace CZ.PAEZ.SaleManager.ListPlugIns
{
    [Description("借用/售后申请单排队")]
    [HotUpdate]
    public class NonsaleApplyQueue : AbstractListPlugIn
    {
        private string filterStr = string.Empty;
        private string orderStr = string.Empty;

        public override void PrepareFilterParameter(FilterArgs e)
        {
            base.PrepareFilterParameter(e);
            if (!filterStr.Equals(""))
            {
                e.AppendQueryFilter(filterStr);
                e.AppendQueryOrderby(orderStr);
            }
        }

        public override void AfterBindData(EventArgs e)
        {
            base.AfterBindData(e);
            // 组织非上手时，隐藏排队菜单
            if (Context.CurrentOrganizationInfo.ID != 100008)
            {
                this.View.GetMainBarItem("PAEZ_tbSplitButton").Visible = false;
            }
        }

        public override void AfterBarItemClick(AfterBarItemClickEventArgs e)
        {
            base.AfterBarItemClick(e);
            string key = e.BarItemKey.ToUpperInvariant();
            string sql;
            switch (key)
            {
                case "PAEZ_TBNAQUEUE":   // PAEZ_tbNaQueue 借用排队
                    QueueLog.Log(Context, "借用/售后申请单", "借用排队");
                    sql = "exec proc_czly_SaleOrderQueue";
                    DBUtils.Execute(Context, sql);
                    filterStr = "FDocumentStatus='C' AND F_PAEZ_IsAllowDelv=1 AND FBillTypeID='602d00ea7fe711'";
                    orderStr = "F_PAEZ_Priority DESC, FDate ASC";
                    this.View.RefreshByFilter();
                    break;
                case "PAEZ_TBASQUEUE":   // PAEZ_tbAsQueue 售后排队
                    QueueLog.Log(Context, "借用/售后申请单", "售后排队");
                    sql = "exec proc_czly_SaleOrderQueue";
                    DBUtils.Execute(Context, sql);
                    filterStr = "FDocumentStatus='C' AND F_PAEZ_IsAllowDelv=1 AND FBillTypeID='602d01347fe889'";
                    orderStr = "F_PAEZ_Priority DESC, FDate ASC";
                    this.View.RefreshByFilter();
                    break;
                case "PAEZ_TBOTHERQUEUE":   // PAEZ_tbOtherQueue 其他排队
                    QueueLog.Log(Context, "借用/售后申请单", "其他排队");
                    sql = "exec proc_czly_SaleOrderQueue";
                    DBUtils.Execute(Context, sql);
                    filterStr = "FDocumentStatus='C' AND F_PAEZ_IsAllowDelv=1 AND FBillTypeID NOT IN ('602d00ea7fe711','602d01347fe889')";
                    orderStr = "F_PAEZ_Priority DESC, FDate ASC";
                    this.View.RefreshByFilter();
                    break;
                case "PAEZ_TBALL": // PAEZ_tbAll 全部单据
                    filterStr = "";
                    orderStr = "";
                    this.View.RefreshByFilter();
                    break;
            }
            
        }
    }
}
