﻿using CZ.PAEZ.SaleManager.Util;
using Kingdee.BOS.App.Data;
using Kingdee.BOS.Core.DynamicForm.PlugIn.Args;
using Kingdee.BOS.Core.List.PlugIn;
using Kingdee.BOS.Core.List.PlugIn.Args;
using Kingdee.BOS.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace CZ.PAEZ.SaleManager.ListPlugIns
{
    [Description("生产订单列表排队")]
    [HotUpdate]
    public class PrdMoQueue : AbstractListPlugIn
    {
        private string filterStr = string.Empty;
        private string orderStr = string.Empty;

        public override void PrepareFilterParameter(FilterArgs e)
        {
            base.PrepareFilterParameter(e);
            if (!filterStr.Equals(""))
            {
                e.AppendQueryFilter(filterStr);
                e.AppendQueryOrderby(orderStr);
            }
        }

        public override void AfterBindData(EventArgs e)
        {
            base.AfterBindData(e);
            // 组织非上手时，隐藏排队菜单
            if (Context.CurrentOrganizationInfo.ID != 100008)
            {
                this.View.GetMainBarItem("PAEZ_tbSplitButton").Visible = false;
            }
        }

        public override void AfterBarItemClick(AfterBarItemClickEventArgs e)
        {
            base.AfterBarItemClick(e);
            string key = e.BarItemKey.ToUpperInvariant();
            string sql;
            switch (key)
            {
                case "PAEZ_TBQUEUE":   // PAEZ_tbQueue 排队
                    QueueLog.Log(Context, "生产订单", "排队");
                    sql = "exec proc_czly_SaleOrderQueue";
                    DBUtils.Execute(Context, sql);
                    filterStr = "FDocumentStatus='C' AND FPickMtrlStatus=1 AND F_PAEZ_IsAllowDelv=1";
                    orderStr = "F_PAEZ_Priority DESC, FDate ASC";
                    this.View.RefreshByFilter();
                    break;
                case "PAEZ_TBALL": // PAEZ_tbAll 全部单据
                    filterStr = "";
                    orderStr = "";
                    this.View.RefreshByFilter();
                    break;
            }
            
        }
    }
}
