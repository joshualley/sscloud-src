﻿using CZ.PAEZ.SaleManager.Util;
using Kingdee.BOS.App.Data;
using Kingdee.BOS.Core.DynamicForm.PlugIn.Args;
using Kingdee.BOS.Core.List.PlugIn;
using Kingdee.BOS.Core.List.PlugIn.Args;
using Kingdee.BOS.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace CZ.PAEZ.SaleManager.ListPlugIns
{
    [Description("销售订单排队计算是否允许下推提货联")]
    [HotUpdate]
    public class SaleOrderQueue : AbstractListPlugIn
    {
        private string filterStr = string.Empty;
        private string orderStr = string.Empty;

        public override void PrepareFilterParameter(FilterArgs e)
        {
            base.PrepareFilterParameter(e);
            if(!filterStr.Equals(""))
            {
                e.AppendQueryFilter(filterStr);
                e.AppendQueryOrderby(orderStr);
            }
        }

        public override void AfterBindData(EventArgs e)
        {
            base.AfterBindData(e);
            // 组织非上手时，隐藏排队菜单
            if(Context.CurrentOrganizationInfo.ID != 100008)
            {
                this.View.GetMainBarItem("PAEZ_tbSplitButton").Visible = false;
            }
        }

        public override void AfterBarItemClick(AfterBarItemClickEventArgs e)
        {
            base.AfterBarItemClick(e);
            string key = e.BarItemKey.ToUpperInvariant();
            string sql;
            switch(key)
            {
                case "PAEZ_TBNORMDELV":   // PAEZ_tbNormDelv 正常发货
                    QueueLog.Log(Context, "销售订单", "正常发货");
                    sql = "exec proc_czly_SaleOrderQueue";
                    DBUtils.Execute(Context, sql);
                    filterStr = "FDocumentStatus='C' AND FMRPCLOSESTATUS='A' AND F_PAEZ_IsAllowDelv=1 AND F_PAEZ_FMkt NOT IN ('T3','T4','T5','D0') AND F_PAEZ_DUIXIAN<>1";
                    orderStr = "F_PAEZ_Priority DESC, FDate ASC, F_PAEZ_ChuPiao ASC";
                    this.View.RefreshByFilter();
                    break;
                case "PAEZ_TBCUSTOMIZATION":   // PAEZ_tbCustomization 定制件
                    QueueLog.Log(Context, "销售订单", "定制件");
                    sql = "exec proc_czly_SaleOrderQueue";
                    DBUtils.Execute(Context, sql);
                    filterStr = "FDocumentStatus='C' AND FMRPCLOSESTATUS='A' AND F_PAEZ_IsAllowDelv=1 AND F_PAEZ_FMkt='D0'";
                    orderStr = "F_PAEZ_Priority DESC, FDate ASC, F_PAEZ_ChuPiao ASC";
                    this.View.RefreshByFilter();
                    break;
                case "PAEZ_TBEXPRESSAGE":   // PAEZ_tbExpressage 快递
                    QueueLog.Log(Context, "销售订单", "快递");
                    sql = "exec proc_czly_SaleOrderQueue";
                    DBUtils.Execute(Context, sql);
                    filterStr = "FDocumentStatus='C' AND FMRPCLOSESTATUS='A' AND F_PAEZ_IsAllowDelv=1 AND F_PAEZ_FMkt IN ('T3','T4')";
                    orderStr = "F_PAEZ_Priority DESC, FDate ASC, F_PAEZ_ChuPiao ASC";
                    this.View.RefreshByFilter();
                    break;
                case "PAEZ_TBSELFPICKUP":   // PAEZ_tbSelfPickUp 自提
                    QueueLog.Log(Context, "销售订单", "自提");
                    sql = "exec proc_czly_SaleOrderQueue";
                    DBUtils.Execute(Context, sql);
                    filterStr = "FDocumentStatus='C' AND FMRPCLOSESTATUS='A' AND F_PAEZ_IsAllowDelv=1 AND F_PAEZ_FMkt='T5'";
                    orderStr = "F_PAEZ_Priority DESC, FDate ASC, F_PAEZ_ChuPiao ASC";
                    this.View.RefreshByFilter();
                    break;
                case "PAEZ_TBFOREIGNTRADE":   // PAEZ_tbForeignTrade 外贸
                    QueueLog.Log(Context, "销售订单", "外贸");
                    sql = "exec proc_czly_SaleOrderQueue";
                    DBUtils.Execute(Context, sql);
                    // 订单对象为1的都是外贸订单
                    filterStr = "FDocumentStatus='C' AND FMRPCLOSESTATUS='A' AND F_PAEZ_IsAllowDelv=1 AND F_PAEZ_DUIXIAN=1";
                    orderStr = "F_PAEZ_Priority DESC, FDate ASC, F_PAEZ_ChuPiao ASC";
                    this.View.RefreshByFilter();
                    break;
                case "PAEZ_TBALL": // PAEZ_tbAll 全部订单
                    filterStr = "";
                    orderStr = "";
                    this.View.RefreshByFilter();
                    break;
            }
        }

        

    }
}
