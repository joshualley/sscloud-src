﻿using Kingdee.BOS.App.Data;
using Kingdee.BOS.Core.DynamicForm.PlugIn;
using Kingdee.BOS.Core.DynamicForm.PlugIn.Args;
using Kingdee.BOS.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace CZ.PAEZ.SaleManager.ServicePlugIns
{
    [Description("发货通知单保存服务")]
    [HotUpdate]

    public class DeliverNotifySave : AbstractOperationServicePlugIn
    {
        public override void AfterExecuteOperationTransaction(AfterExecuteOperationTransaction e)
        {
            base.AfterExecuteOperationTransaction(e);
            foreach (var dataEntity in e.DataEntitys)
            {
                // 将携带的编号写入到单据编号上
                string fid = dataEntity["Id"].ToString();
                string sql = $"/*dialect*/update T_SAL_DELIVERYNOTICE set FBillNo=F_PAEZ_billno where FID='{fid}'";
                DBUtils.Execute(Context, sql);
            }
        }
    }
}
