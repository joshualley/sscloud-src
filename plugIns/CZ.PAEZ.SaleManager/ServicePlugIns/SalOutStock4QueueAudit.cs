﻿using Kingdee.BOS.App.Data;
using Kingdee.BOS.Core.DynamicForm.PlugIn;
using Kingdee.BOS.Core.DynamicForm.PlugIn.Args;
using Kingdee.BOS.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace CZ.PAEZ.SaleManager.ServicePlugIns
{
    /// <summary>
    /// 未完成，且未启用
    /// </summary>
    [Description("销售出库审核")]
    [HotUpdate]
    public class SalOutStock4QueueAudit : AbstractOperationServicePlugIn
    {
        public override void EndOperationTransaction(EndOperationTransactionArgs e)
        {
            base.EndOperationTransaction(e);
            switch (this.FormOperation.Operation.ToUpperInvariant())
            {
                case "AUDIT":
                    Act_Audit(e);
                    break;
                case "UNAUDIT":
                    Act_UnAudit(e);
                    break;
            }
        }

        private void Act_Audit(EndOperationTransactionArgs e)
        {
            foreach(var dataEntity in e.DataEntitys)
            {
                string fid = dataEntity["Id"].ToString();
                string sql = string.Format(@"/*dialect*/
update oe set 
from T_SAL_ORDERENTRY oe
inner join (
    select * from (
        select oe.FEntryId, oe.FQty, sum(ose.FRealQty) FRealQty from T_SAL_OUTSTOCKENTRY ose
        inner join T_SAL_OUTSTOCKENTRY_LK oselk on oselk.FEntryId=ose.FENTRYID and oselk.FSTableName='T_SAL_DELIVERYNOTICEENTRY'
        inner join T_SAL_DELIVERYNOTICEENTRY_LK delk on delk.FENTRYID=oselk.FSId and delk.FSTableName='PAEZ_SaleoutSumEntry'
        inner join PAEZ_SaleoutSumEntry_LK selk on selk.FEntryID=delk.FSId and selk.FSTableName='PAEZ_t_LadingEntry'
        inner join PAEZ_t_LadingEntry_LK lelk on lelk.FEntryID=selk.FSId and lelk.FSTableName='T_SAL_ORDERENTRY'
        inner join T_SAL_ORDERENTRY oe on oe.FEntryId=lelk.FSId
        where oe.FEntryId in (
            select distinct oe.FEntryId from T_SAL_OUTSTOCKENTRY ose
            inner join T_SAL_OUTSTOCKENTRY_LK oselk on oselk.FEntryId=ose.FENTRYID and oselk.FSTableName='T_SAL_DELIVERYNOTICEENTRY'
            inner join T_SAL_DELIVERYNOTICEENTRY_LK delk on delk.FENTRYID=oselk.FSId and delk.FSTableName='PAEZ_SaleoutSumEntry'
            inner join PAEZ_SaleoutSumEntry_LK selk on selk.FEntryID=delk.FSId and selk.FSTableName='PAEZ_t_LadingEntry'
            inner join PAEZ_t_LadingEntry_LK lelk on lelk.FEntryID=selk.FSId and lelk.FSTableName='T_SAL_ORDERENTRY'
            inner join T_SAL_ORDERENTRY oe on oe.FEntryId=lelk.FSId
            where ose.FID={0}
        ) group by oe.FEntryId, oe.FQty
    )t where FQty=FRealQty
)lt on lt.FEntryId=oe.FEntryId
", fid);
                var items = DBUtils.ExecuteDynamicObject(this.Context, sql);

            }
        }

        private void Act_UnAudit(EndOperationTransactionArgs e)
        {
            foreach (var dataEntity in e.DataEntitys)
            {
                string fid = dataEntity["Id"].ToString();
            }
        }
    }
}
