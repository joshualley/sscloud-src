using System;
using System.Linq;
using System.ComponentModel;
using Kingdee.BOS.Util;
using Kingdee.BOS.App.Data;
using Kingdee.BOS.Core.DynamicForm.PlugIn;
using Kingdee.BOS.Core.DynamicForm.PlugIn.Args;
using Kingdee.BOS.Core.Validation;
using Kingdee.BOS.Core;
using Kingdee.BOS;

namespace CZ.PAEZ.SaleManager.ServicePlugIns
{
    [Description("生产订单保存-校验批号唯一")]
    [HotUpdate]
    public class PrdMoOrderSave : AbstractOperationServicePlugIn
    {
        public override void OnAddValidators(AddValidatorsEventArgs e)
        {
            base.OnAddValidators(e);
            var validator = new Validator();
            validator.AlwaysValidate = true;
            validator.EntityKey = "FBillHead";
            e.Validators.Add(validator);
        }

        public class Validator : AbstractValidator
        {
            public override void Validate(ExtendedDataEntity[] dataEntities, ValidateContext validateContext, Context ctx)
            {
                foreach (var item in dataEntities)
                {
                    CheckSingleOrder(item, validateContext, ctx);
                }
            }

            private void CheckSingleOrder(ExtendedDataEntity dataEntity, ValidateContext validateContext, Context ctx)
            {
                string fid = dataEntity["Id"].ToString();
                string sql = $"select FBillType from T_PRD_MO where FID={fid}";
                var item = DBUtils.ExecuteDynamicObject(Context, sql).FirstOrDefault();
                if (item == null) return;

                string FBillType = item["FBillType"].ToString();
                // 汇报入库-返工生产 || 工序汇报入库-返工生产
                if ("0e74146732c24bec90178b6fe16a2d1c".Equals(FBillType) || "5f93b1da667404".Equals(FBillType)) return;

                sql = $"select FMaterialId, FLot_Text from T_PRD_MOENTRY where FID={fid}";
                var items = DBUtils.ExecuteDynamicObject(Context, sql)
                    .Select(x => new 
                    {
                        FMaterialId = x["FMaterialId"].ToString(),
                        FLot_Text = x["FLot_Text"].ToString(),
                    }).ToArray();
                
                for (int i = 0; i < items.Length-1; i++)
                {
                    // 1、检查已保存订单中是否有物料-批号重复的行
                    sql = string.Format(@"select FEntryId from T_PRD_MOENTRY me
                        inner join T_BD_LotMaster lm on lm.FLOTID=me.FLot
                        where me.FMaterialId={0} and lm.FNumber='{1}' and me.FID<>{2}", 
                        items[i].FMaterialId, items[i].FLot_Text, fid);
                    var result = DBUtils.ExecuteDynamicObject(Context, sql);
                    if (result.Count > 0)
                    {
                        ValidationErrorInfo ValidationErrorInfo = new ValidationErrorInfo(
                            string.Empty,
                            fid,
                            dataEntity.DataEntityIndex,
                            dataEntity.RowIndex,
                            fid,
                            $"第{i+1}行中的物料和批号在生产订单中已经存在！",
                            string.Empty);
                        validateContext.AddError(null, ValidationErrorInfo);
                        continue;
                    }

                    // 2、检查本单中该行物料-批号是否重复
                    for (int j = i + 1; j < items.Length; j++)
                    {
                        if (items[i].FMaterialId.Equals(items[j].FMaterialId) && 
                            items[i].FLot_Text.Equals(items[j].FLot_Text))
                        {
                            ValidationErrorInfo ValidationErrorInfo = new ValidationErrorInfo(
                                string.Empty,
                                fid,
                                dataEntity.DataEntityIndex,
                                dataEntity.RowIndex,
                                fid,
                                $"第{i+1}和{j+1}行的物料和批号重复！",
                                string.Empty);
                            validateContext.AddError(null, ValidationErrorInfo);
                        }
                    }
                }
            }
        }

        
    }
}
