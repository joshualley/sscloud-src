using System;
using System.Linq;
using System.ComponentModel;
using System.Collections.Generic;
using Kingdee.BOS.Util;
using Kingdee.BOS.App.Data;
using Kingdee.BOS.Core.DynamicForm.PlugIn;
using Kingdee.BOS.Core.DynamicForm.PlugIn.Args;


namespace CZ.PAEZ.SaleManager.ServicePlugIns
{
    [Description("检验单审核插件")]
    [HotUpdate]
    public class InspectBillAudit : AbstractOperationServicePlugIn
    {
        public override void EndOperationTransaction(EndOperationTransactionArgs e)
        {
            base.EndOperationTransaction(e);
            // 审核后将检验单单号反写至关联的发货通知单
            foreach(var dataEntity in e.DataEntitys)
            {
                string fid = dataEntity["Id"].ToString();
                string sql = string.Format(@"/*dialect*/
update dn set F_PAEZ_JYDH=dnt.FBillNo from T_SAL_DELIVERYNOTICE dn
inner join (
    select distinct dne.FID, ib.FBillNo
    from T_SAL_DELIVERYNOTICEENTRY dne
    inner join T_QM_STKAPPINSPECTENTRY_LK ailk on ailk.FENTRYID=dne.FENTRYID and ailk.FSTableName='T_SAL_DELIVERYNOTICEENTRY'
    inner join T_QM_INSPECTBILLENTRY_LK iblk on iblk.FSID=ailk.FENTRYID and iblk.FSTableName='T_QM_STKAPPINSPECTENTRY'
    inner join T_QM_INSPECTBILLENTRY ibe on ibe.FEntryId=iblk.FSID
    inner join T_QM_INSPECTBILL ib on ib.FID=ibe.FID
    where ib.FID={0}
) dnt on dnt.FID=dn.FID", fid);
                DBUtils.Execute(Context, sql);
            }
        }
    }
}
