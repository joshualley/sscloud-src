using System;
using System.Linq;
using System.ComponentModel;
using System.Collections.Generic;
using Kingdee.BOS.Util;
using Kingdee.BOS.App.Data;
using Kingdee.BOS.Core.Bill.PlugIn;
using Kingdee.BOS.Core.Bill.PlugIn.Args;
using Kingdee.BOS.Core.DynamicForm.PlugIn;
using Kingdee.BOS.Core.DynamicForm.PlugIn.Args;


namespace CZ.PAEZ.SaleManager.ServicePlugIns
{
    [Description("生产用料清单审核插件")]
    [HotUpdate]
    public class PPBomAudit : AbstractOperationServicePlugIn
    {
        public override void EndOperationTransaction(EndOperationTransactionArgs e)
        {
            base.EndOperationTransaction(e);
            // 审核时将用料清单中编码以_S结尾的物料的批号刷为生产订单对应行上的批号
            foreach(var dataEntity in e.DataEntitys)
            {
                string fid = dataEntity["Id"].ToString();
                string sql = string.Format(@"/*dialect*/
update pbec set pbec.FLot=lm.FLotId, pbec.FLot_Text=lm.FNumber
from T_PRD_PPBOMENTRY_C pbec
inner join T_PRD_PPBOMENTRY pbe on pbec.FEntryId=pbe.FEntryId
inner join T_PRD_PPBOM pb on pb.FID=pbe.FID and pb.FID={0}
inner join T_BD_Material m on m.FMaterialId=pbe.FMATERIALID
inner join T_BD_LotMaster lm on lm.FMaterialID=m.FMasterId 
    and lm.FUseOrgId=m.FUSEORGID and lm.FBizType=1
    and lm.FNumber=(select FLot_Text from T_PRD_MOENTRY me where FEntryId=pb.FMOEntryID)
where right(m.FNumber, 2)='_S'
", fid);
                DBUtils.Execute(Context, sql);
            }
        }
    }
}
