﻿using Kingdee.BOS.Core.DynamicForm.PlugIn;
using Kingdee.BOS.Core.DynamicForm.PlugIn.Args;
using Kingdee.BOS.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace CZ.PAEZ.SaleManager.FormPlugIns
{
    [Description("金税盘选择")]
    [HotUpdate]
    public class GoldTakDiskChoice : AbstractDynamicFormPlugIn
    {
        private string mTaxDiskNo = "";
        public override void AfterBarItemClick(AfterBarItemClickEventArgs e)
        {
            base.AfterBarItemClick(e);
            switch (e.BarItemKey.ToUpperInvariant())
            {
                case "PAEZ_TBCONFIRM": // PAEZ_tbConfirm
                    mTaxDiskNo = this.Model.GetValue("FTaxDiskNo")?.ToString();
                    this.View.ReturnToParentWindow(mTaxDiskNo);
                    this.View.Close();
                    break;
            }
        }

        public override void BeforeClosed(BeforeClosedEventArgs e)
        {
            base.BeforeClosed(e);
            if (mTaxDiskNo.IsNullOrEmptyOrWhiteSpace())
            {
                this.View.ReturnToParentWindow(null);
            }
        }
    }
}
