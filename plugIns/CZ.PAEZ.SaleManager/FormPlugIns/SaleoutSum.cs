﻿using Kingdee.BOS.Core.Bill.PlugIn;
using System.ComponentModel;
using Kingdee.BOS.Util;
using Kingdee.BOS.Core.DynamicForm.PlugIn.Args;

namespace CZ.PAEZ.SaleManager.FormPlugIns
{
    [Description("发货汇总单")]
    [HotUpdate]
    public class SaleoutSum : AbstractBillPlugIn
    {
        public override void AfterBindData(System.EventArgs e)
        {
            base.AfterBindData(e);
            string FDocumentStatus = this.Model.GetValue("FDocumentStatus")?.ToString();
            if ("Z".Equals(FDocumentStatus))
            {
                // 计算发票数量
                string FInvoice = this.Model.GetValue("FInvoice")?.ToString();
                if (!FInvoice.IsNullOrEmptyOrWhiteSpace())
                {
                    this.Model.SetValue("F_PAEZ_InVoiceQty", FInvoice.Split(',').Length);
                }
            }
        }


        public override void DataChanged(DataChangedEventArgs e)
        {
            base.DataChanged(e);
            switch (e.Field.Key)
            {
                case "FInvoice": // 发票号
                    // 计算发票数量
                    this.Model.SetValue("F_PAEZ_InVoiceQty", e.NewValue.ToString().Split(',').Length);
                    break;
            }
        }

        public override void AfterDoOperation(AfterDoOperationEventArgs e)
        {
            base.AfterDoOperation(e);
            switch (e.Operation.Operation.ToUpperInvariant())
            {
                case "SAVE":
                    this.View.Refresh();
                    break;
            }
        }

    }
}
