# 携带供应商邮箱
import clr
clr.AddReference('Kingdee.BOS.App')
from Kingdee.BOS.App.Data import *

def DataChanged(e):
    key = str(e.Field.Key).upper()
    if key == "F_PAEZ_SUPPLIER":
        FSupplierId = e.NewValue
        if FSupplierId is None:
            return
        sql = """select top 1 FEMail from t_BD_SupplierContact 
        where FSupplierId={} and FEMail<>''""".format(FSupplierId)
        dt = DBUtils.ExecuteDataSet(this.Context, sql).Tables[0]
        if dt.Rows.Count <= 0:
            return
        this.Model.SetValue("F_PAEZ_EMail", dt.Rows[0]["FEMail"])
        