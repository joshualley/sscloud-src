# 销售订单审核时写入排队日期

import clr
clr.AddReference('Kingdee.BOS.App')
from Kingdee.BOS.App.Data import *
from System import DateTime


def AfterExecuteOperationTransaction(e):
    for dataEntity in e.DataEntitys:
        fid = dataEntity["Id"]
        now = DateTime.Now
        sql = "update T_SAL_ORDER set F_PAEZ_QueueDt='{}' where FID={}".format(now, fid)
        DBUtils.Execute(this.Context, sql)
