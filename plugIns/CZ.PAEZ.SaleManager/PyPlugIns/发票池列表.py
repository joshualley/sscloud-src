# 发票池标记为已经使用

import clr
clr.AddReference('Kingdee.BOS.App')
clr.AddReference('Kingdee.BOS.Core')
from Kingdee.BOS.App.Data import *
from Kingdee.BOS.Core.DynamicForm import MessageBoxOptions, MessageBoxResult


def AfterBarItemClick(e):
    key = e.BarItemKey.upper()
    if key == "PAEZ_TBTAGUSED": # PAEZ_tbTagUsed
        rows = this.ListView.SelectedRowsInfo
        if rows.Count <= 0:
            return
        ids = ",".join(set([row.PrimaryKeyValue for row in rows]))
        def TagForm(r):
            if r != MessageBoxResult.Yes:
                return
            sql = """/*dialect*/
            update ip set ip.FIsUsed=1 from PAEZ_BD_InvoicePool ip
            where FID in ({})
            """.format(ids)
            DBUtils.Execute(this.Context, sql)
        
        this.View.ShowMessage("确定将发票标记为已经使用吗？", MessageBoxOptions.YesNo, TagForm)
    elif key == "PAEZ_TBUNTAGUSED": # PAEZ_tbUnTagUsed
        rows = this.ListView.SelectedRowsInfo
        if rows.Count <= 0:
            return
        ids = ",".join(set([row.PrimaryKeyValue for row in rows]))
        def UnTagForm(r):
            if r != MessageBoxResult.Yes:
                return
            sql = """/*dialect*/
            update ip set ip.FIsUsed=0 from PAEZ_BD_InvoicePool ip
            where FID in ({})
            """.format(ids)
            DBUtils.Execute(this.Context, sql)
        
        this.View.ShowMessage("确定将发票标记为未使用吗？", MessageBoxOptions.YesNo, TagForm)