# 判定是否合格

def DataChanged(e):
    key = str(e.Field.Key)
    if key == 'F_PAEZ_ZLBPD':
        F_PAEZ_ZLBPD = this.Model.GetValue("F_PAEZ_ZLBPD")
        if F_PAEZ_ZLBPD is None:
            return
        fid = str(F_PAEZ_ZLBPD['Id'])
        value = '1' if fid == '603095f8d651f7' else '2' if fid == '60309607d651f9' else ''
        count = this.Model.GetEntryRowCount("FEntity")
        for i in range(count):
            FQualifiedQty = 0 if this.Model.GetValue("FQualifiedQty", i) is None else str(this.Model.GetValue("FQualifiedQty", i))
            this.Model.SetValue("FInspectResult", value, i)
            this.Model.SetValue("FQualifiedQty", FQualifiedQty, i)


