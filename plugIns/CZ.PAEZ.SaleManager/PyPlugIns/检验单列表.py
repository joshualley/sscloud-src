# 优科：将列表中所选单据标为不良品

import clr
clr.AddReference('Kingdee.BOS.App')
clr.AddReference('Kingdee.BOS.Core')
from Kingdee.BOS.App.Data import *
from Kingdee.BOS.Core.DynamicForm import MessageBoxOptions, MessageBoxResult


def AfterBarItemClick(e):
    key = e.BarItemKey.upper()
    if key == "PAEZ_TBTAGREJECTS":  # PAEZ_tbTagRejects 标为不良品
        rows = this.ListView.SelectedRowsInfo
        if rows.Count <= 0:
            return
        ids = ",".join(set([row.PrimaryKeyValue for row in rows]))
        def TagForm(r):
            if r != MessageBoxResult.Yes:
                return
            sql = """/*dialect*/
            update t3 set t3.FISDEFECTPROCESS=1 from T_QM_INSPECTBILL t1 
            inner join T_QM_INSPECTBILLENTRY t2 on t2.fid=t1.fid and t2.FUNQUALIFIEDQTY<>0
            inner join T_QM_IBPOLICYDETAIL t3 on t3.FENTRYID=t2.FENTRYID 
            where t1.FINSPECTORGID=100009 and t3.FUSEPOLICY='i' and t1.FID in ({})
            """.format(ids)
            DBUtils.Execute(this.Context, sql)
        
        this.View.ShowMessage("确定要标记这些单据吗？", MessageBoxOptions.YesNo, TagForm)

