# 费用项目序号类别绑定
import clr
clr.AddReference('Kingdee.BOS.App')
from Kingdee.BOS.App.Data import *


def DataChanged(e):
    key = str(e.Field.Key)
    if key == "F_PAEZ_ClsName":
        v = e.NewValue
        sql = """select distinct F_PAEZ_ClsSeq 
from T_BD_EXPENSE where F_PAEZ_ClsName='{}' and F_PAEZ_ClsSeq<>0""".format(v)
        dt = DBUtils.ExecuteDataSet(this.Context, sql).Tables[0]
        if dt.Rows.Count <= 0:
            return
        this.Model.SetValue("F_PAEZ_ClsSeq", dt.Rows[0]["F_PAEZ_ClsSeq"])
    elif key == "F_PAEZ_ClsSeq":
        v = e.NewValue
        sql = """select distinct F_PAEZ_ClsName 
from T_BD_EXPENSE where F_PAEZ_ClsName<>'' and F_PAEZ_ClsSeq={}""".format(v)
        dt = DBUtils.ExecuteDataSet(this.Context, sql).Tables[0]
        if dt.Rows.Count <= 0:
            return
        this.Model.SetValue("F_PAEZ_ClsName", dt.Rows[0]["F_PAEZ_ClsName"])