-- 研发费用报表
alter proc proc_czly_ResearchFeeRpt(
    @date datetime,
    @orgNo varchar(55)
)as
begin

set nocount on


declare @year int = Year(@date)
declare @orgId int=(select FOrgId from T_ORG_ORGANIZATIONS where FNumber=@orgNo)

select * into #temp from dbo.fun_czly_GetFeeData(@year, '6604', @orgId) order by 序号

select * from #temp
union all
select 0, '合计', 
    sum(一月), sum(二月), sum(三月), sum(四月),
    sum(五月), sum(六月), sum(七月), sum(八月),
    sum(九月), sum(十月), sum(十一月), sum(十二月),
    sum(累计)
from #temp

end

-- exec proc_czly_ResearchFeeRpt @date='2021-1-1', @orgNo='SS'