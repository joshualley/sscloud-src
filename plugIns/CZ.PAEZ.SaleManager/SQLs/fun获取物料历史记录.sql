-- 获取物料历史记录
alter function fun_czly_GetMtlHistoryRecord(
    @number varchar(55),
    @org_id int,
    @date datetime
) returns table
as
return (
    select top 1 FMtlName, FMtlSpecification, FPrintUnit, FRegistrationNo
    from PAEZ_BD_MtlModifyRecord mr
    inner join T_BD_Material m on m.FMaterialId=mr.FMaterialId
    where m.FNumber=@number and m.FUseOrgId=@org_id
    and mr.FEffectiveDate<@date
    order by FEffectiveDate desc
)

-- select * from dbo.fun_czly_GetMtlHistoryRecord('RYY050', 100008, '2021-1-1')
