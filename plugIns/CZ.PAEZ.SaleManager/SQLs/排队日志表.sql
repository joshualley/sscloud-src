
create table PAEZ_T_QueneLog(
    FID bigint primary key identity(1,1),
    FDate datetime,
    FMaterialId bigint,
    FMtlNumber varchar(55),
    FStkQty decimal(18, 2),
    FCanUseQty decimal(18, 2),
    FAllocQty decimal(18, 2) 
)

