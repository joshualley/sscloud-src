alter proc proc_czly_CheckLadingError
    @mtl_number varchar(100),
    @query_type varchar(20)
as
begin
set nocount on

if isnull(@query_type, '')='' set @query_type='1'

select o.FDate, o.FBILLNO FOBillNo, oe.FSeq FOSeq, oe.FQty, l.FBillNo FLBillNo, le.FSeq FLSeq, l.FInvoice, oe.FMATERIALID
-- 获取物料的即时库存
into #info
from dbo.fun_czly_GetMtlStkOcupy(@mtl_number) i
inner join t_sal_OrderEntry oe on oe.FMATERIALID=i.FMATERIALID
inner join t_sal_Order o on o.FID=oe.FID
inner join PAEZ_t_LadingEntry_LK lelk on lelk.FSId=oe.FENTRYID
inner join PAEZ_t_LadingEntry le on le.FEntryID=lelk.FEntryID
inner join PAEZ_t_Lading l on l.FID=le.FID and l.FRepeatInvoice=0
left join PAEZ_SaleoutSumEntry_LK selk on selk.FSId=le.FEntryId and selk.FSTableName='PAEZ_t_LadingEntry'
left join T_SAL_DELIVERYNOTICEENTRY_LK delk on delk.FSId=selk.FEntryID and delk.FSTableName='PAEZ_SaleoutSumEntry'
left join T_SAL_OUTSTOCKENTRY_LK oselk on oselk.FSId=delk.FENTRYID and oselk.FSTableName='T_SAL_DELIVERYNOTICEENTRY'
left join T_SAL_OUTSTOCKENTRY ose on ose.FEntryId=oselk.FEntryId
left join T_SAL_OUTSTOCK os on os.FID=ose.FID
where oe.F_PAEZ_PickQty>0 and ISNULL(os.FDocumentStatus, '')<>'C'


select 
    o.FDate 订单日期,
    o.FOBillNo 订单编号, o.FOSeq 订单行,
    i.FNumber 物料编码, o.FQty 销售数量, 
    i.FQtyInv 即时库存, i.FQtyLock 锁库数量,
    i.FNaQty 借用占用数量, i.FAsQty 售后占用数量, 
    i.FLdQty 提货占用数量, i.FQtyProd 领料数量,
    i.FleftStkQty 剩余可用库存, 
    o.FLBillNo 提货联号, o.FLSeq 提货联行, o.FInvoice 发票号
from #info o
inner join dbo.fun_czly_GetMtlStkOcupy(@mtl_number) i on o.FMATERIALID=i.FMATERIALID
where (@query_type='2' OR i.FleftStkQty<0)
order by i.FNumber

drop table #info
end

-- exec proc_czly_CheckLadingError @mtl_number='#FMaterialId#', @query_type='#FQType#'