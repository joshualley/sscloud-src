
-- 判断凭证是否为结转损益
create function fun_czly_IsCarryForward(
    @voucherId int
) returns int
as
begin 
    declare @r int=0
    if (select count(*) from V_CN_VOUCHERENTRY where FVoucherId=@voucherId and FEXPLANATION='结转本期损益')>0 
    begin
        set @r = 1
    end
    
    return @r
end