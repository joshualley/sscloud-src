-- 核对销售发货线上各单据中的数量
create proc proc_czly_CheckSaleDelvQtyNoR(
    @date datetime,
    @is_qty_diff int
) as
begin
set nocount on

if isnull(@date, '')='' set @date = getdate()

select l.FMaterialId, l.FCustNo, l.FCustName, l.FNumber,
    FQtyLd, 
    isnull(FQtySum, 0) FQtySum, 
    isnull(FQtyDn, 0) FQtyDn, 
    isnull(FQtyOs, 0) FQtyOs, 
    isnull(FQtySc, 0) FQtySc
from (
    -- 提货联
    select le.FMaterialId, m.FNumber, c.FCustId, c.FNumber FCustNo, cl.FName FCustName,
        SUM(le.FQty) FQtyLd
    from PAEZ_t_Lading l
    inner join T_BD_CUSTOMER c on c.FCustId=l.FCustomerID
    inner join T_BD_CUSTOMER_L cl on cl.FCustId=c.FCustId
    inner join PAEZ_t_LadingEntry le on l.FID=le.FID and le.F_PAEZ_YWZF=0
    inner join T_BD_MATERIAL m on m.FMaterialId=le.FMaterialId
    where l.FDocumentStatus='C' and l.FRepeatInvoice=0
    and year(l.FDate)=year(@date) and month(l.FDate)=month(@date)
    group by le.FMaterialId, m.FNumber, c.FCustId, c.FNumber, cl.FName
) l 
left join (
    -- 发货汇总
    select FMaterialId, FCustId, SUM(FQty) FQtySum from (
        select distinct se.FEntryId, se.FMaterialId, c.FCustId, se.FQty
        from PAEZ_t_Lading l
        inner join T_BD_CUSTOMER c on c.FCustId=l.FCustomerID
        inner join PAEZ_t_LadingEntry le on l.FID=le.FID and le.F_PAEZ_YWZF=0
        inner join PAEZ_SaleoutSumEntry_LK selk on selk.FSId=le.FEntryId and selk.FSTableName='PAEZ_t_LadingEntry'
        inner join PAEZ_SaleoutSumEntry se on se.FEntryId=selk.FEntryId
        inner join PAEZ_SaleoutSum s on s.FID=se.FID
        where s.FDocumentStatus='C' and l.FRepeatInvoice=0
        and year(l.FDate)=year(@date) and month(l.FDate)=month(@date)
    ) se
    group by FMaterialId, FCustId
) s on s.FMaterialId=l.FMaterialId and s.FCustId=l.FCustId
left join (
    -- 发货通知
    select FMaterialId, FCustId, SUM(FQty) FQtyDn from (
        select distinct dne.FEntryId, dne.FMaterialId, c.FCustId, dne.FQty
        from PAEZ_t_Lading l
        inner join T_BD_CUSTOMER c on c.FCustId=l.FCustomerID
        inner join PAEZ_t_LadingEntry le on l.FID=le.FID and le.F_PAEZ_YWZF=0
        inner join PAEZ_SaleoutSumEntry_LK selk on selk.FSId=le.FEntryId and selk.FSTableName='PAEZ_t_LadingEntry'
        inner join T_SAL_DELIVERYNOTICEENTRY_LK delk on delk.FSId=selk.FEntryID and delk.FSTableName='PAEZ_SaleoutSumEntry'
        inner join T_SAL_DELIVERYNOTICEENTRY dne on dne.FEntryId=delk.FEntryId
        inner join T_SAL_DELIVERYNOTICE dn on dn.FID=dne.FID
        where dn.FDocumentStatus='C' and l.FRepeatInvoice=0
        and year(l.FDate)=year(@date) and month(l.FDate)=month(@date)
    ) dne
    group by FMaterialId, FCustId
) d on d.FMaterialId=l.FMaterialId and d.FCustId=l.FCustId
left join (
    -- 发货出库
    select FMaterialId, FCustId, SUM(FRealQty) FQtyOs from (
        select distinct ose.FEntryId, ose.FMaterialId, c.FCustId, ose.FRealQty 
        from PAEZ_t_Lading l
        inner join T_BD_CUSTOMER c on c.FCustId=l.FCustomerID
        inner join PAEZ_t_LadingEntry le on l.FID=le.FID and le.F_PAEZ_YWZF=0
        inner join PAEZ_SaleoutSumEntry_LK selk on selk.FSId=le.FEntryId and selk.FSTableName='PAEZ_t_LadingEntry'
        inner join T_SAL_DELIVERYNOTICEENTRY_LK delk on delk.FSId=selk.FEntryID and delk.FSTableName='PAEZ_SaleoutSumEntry'
        inner join T_SAL_OUTSTOCKENTRY_LK oselk on oselk.FSId=delk.FENTRYID and oselk.FSTableName='T_SAL_DELIVERYNOTICEENTRY'
        inner join T_SAL_OUTSTOCKENTRY ose on ose.FEntryId=oselk.FEntryId
        inner join T_SAL_OUTSTOCK os on os.FID=ose.FID
        where os.FDocumentStatus='C' and l.FRepeatInvoice=0
        and year(l.FDate)=year(@date) and month(l.FDate)=month(@date)
    ) ose
    group by FMaterialId, FCustId
) os on os.FMaterialId=l.FMaterialId and os.FCustId=l.FCustId
left join (
    -- 发票
    select sce.FMaterialId, c.FCustId, SUM(sce.FPRICEQTY) FQtySc 
    from PAEZ_t_Lading l
    inner join T_BD_CUSTOMER c on c.FCustId=l.FCustomerID
    inner join (
        select distinct FID, FEntryId, FMaterialId, FPRICEQTY, FDocumentStatus from (
            select le.FID, le.FEntryId, sce.FMaterialId, sce.FPRICEQTY, sc.FDocumentStatus
            from PAEZ_t_LadingEntry le
            inner join T_IV_SALESICENTRY_LK scelk on scelk.FSId=le.FEntryId and scelk.FSTableName='PAEZ_t_LadingEntry'
            inner join T_IV_SALESICENTRY sce on sce.FEntryId=scelk.FEntryId
            inner join T_IV_SALESIC sc on sc.FID=sce.FID
            inner join T_IV_SALESIC_O sco on sco.FID=sc.FID
            where le.F_PAEZ_YWZF=0
            union all 
            select le.FID, le.FEntryId, sce.FMaterialId, sce.FPRICEQTY, sc.FDocumentStatus
            from PAEZ_t_LadingEntry le
            inner join t_AR_receivableEntry_LK relk on relk.FSId=le.FEntryId and relk.FSTableName='PAEZ_t_LadingEntry'
            inner join T_IV_SALESICENTRY_LK scelk on scelk.FSId=relk.FEntryId and scelk.FSTableName='t_AR_receivableEntry'
            inner join T_IV_SALESICENTRY sce on sce.FEntryId=scelk.FEntryId
            inner join T_IV_SALESIC sc on sc.FID=sce.FID
            inner join T_IV_SALESIC_O sco on sco.FID=sc.FID
            where le.F_PAEZ_YWZF=0
        )t
    ) sce on l.FID=sce.FID
    where sce.FDocumentStatus='C'
    and l.FRepeatInvoice=0 and year(l.FDate)=year(@date) and month(l.FDate)=month(@date)
    group by sce.FMaterialId, c.FCustId
) sc on sc.FMaterialId=l.FMaterialId and sc.FCustId=l.FCustId
where @is_qty_diff=0 or isnull(FQtyOs, 0)<>isnull(FQtySc, 0)
order by l.FCustNo

end

-- exec proc_czly_CheckSaleDelvQty '#FDate#', '#FQtyDiff#'

