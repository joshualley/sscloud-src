-- 获取物料的库存
alter function fun_czly_GetMtlStkOcupy(
    @mtl_number varchar(100)
) 
returns table
as
return (
    select 
        inv.FMaterialId, 
        inv.FNumber,
        inv.FQtyInv,                            -- 即时库存
        isnull(ll.FQtyLockLot, 0) FQtyLockLot,  -- 批号锁库数量
        isnull(lk.FQtyLock, 0) FQtyLock,        -- 锁库
        isnull(po.FQtyProd, 0) FQtyProd,        -- 生产领料
        isnull(nai.FQty, 0) FNaQty,             -- 借用占用
        isnull(asi.FQty, 0) FAsQty,             -- 售后占用
        isnull(ldi.FQty, 0) FLdQty,             -- 提货联占用
        inv.FQtyInv-isnull(lk.FQtyLock, 0)
            -isnull(ll.FQtyLockLot, 0)
            -isnull(po.FQtyProd, 0)
            -isnull(nai.FQty, 0)
            -isnull(asi.FQty, 0)
            -isnull(ldi.FQty, 0) FleftStkQty --可用库存
    from(
        select m.FMaterialId,m.FNumber,sum(iv.FBaseQty)FQtyInv ---into #inv
        --from(select distinct FMaterialID FMtlID from t_sal_OrderEntry where FID=107007)sm 
        from (select FMaterialID, FMasterID, FNumber from t_bd_Material where FUseOrgId=100008) m
        inner join t_STK_Inventory iv on m.FMasterID=iv.FMaterialID and iv.FStockOrgID=100008
        inner join t_bd_stock sk on iv.FStockID=sk.FStockID and sk.FNumber like('CK003%')
        group by m.FMaterialId, m.FNumber
    )inv
    left join(
        select m.FMaterialID, sum(iv.FBaseQty) FQtyLockLot
        from (select FMaterialID, FMasterID, FNumber from t_bd_Material where FUseOrgId=100008) m
        inner join t_STK_Inventory iv on m.FMasterID=iv.FMaterialID and iv.FStockOrgID=100008
        inner join t_bd_stock sk on iv.FStockID=sk.FStockID and sk.FNumber like('CK003%')
        inner join t_bd_LotMaster lm on iv.FMaterialID=lm.FMaterialID and iv.FLot=lm.FLotId 
        inner join PAEZ_Sal_StopSaleEntry se on m.FMaterialID=se.FMtlID and lm.FNumber=se.FLotText 
        inner join PAEZ_Sal_StopSale s on se.FID=s.FID and s.FDocumentStatus='C'
        group by m.FMaterialID 
    )ll on inv.FMaterialId=ll.FMaterialId
    left join (
        -- 减去库存强制保留单
        select le.FMtlID FMaterialId,sum(le.FQty)FQtyLock
        from PAEZ_Stk_LockMtlEntry le
        inner join PAEZ_Stk_LockMtl l on le.FID=l.FID and l.FDocumentStatus='C'
        group by le.FMtlID
    )lk on inv.FMaterialId=lk.FMaterialId
    left join (
        -- 生产领料单未审核占用的库存
        select q.FMaterialId, sum(q.FACTUALQTY) FQtyProd
        from (
            select distinct pd.FEntryID, pd.FMaterialId, pd.FActualQty 
            from T_PRD_MOENTRY moe
            inner join (
                select FMaterialID, FMasterID, FNumber, FMATERIALGROUP from t_bd_Material 
                where FUseOrgId=100008 and FMATERIALGROUP=351757
            ) m on m.FMaterialId=moe.FMaterialId
            inner join T_PRD_PPBOM pb on pb.FMOEntryID=moe.FEntryID
            inner join T_PRD_PPBOMENTRY pbe on pbe.FID=pb.FID
            inner join T_PRD_PICKMTRLDATA pd on pd.FPPBomEntryId=pbe.FEntryId
            inner join T_PRD_PICKMTRL p on p.FID=pd.FID
            where p.FDocumentStatus<>'C'
        )q
        group by q.FMaterialId
    )po on po.FMaterialId=inv.FMaterialId
    left join (
        -- 减去借用申请单未出库的占用数量
        select ne.FMaterialID, sum(ne.FQTY) FQty from (
            select distinct * from (
                select  ne.FEntryID, ne.FMaterialID, ne.FQty, isnull(ti.FDocumentStatus, '') FDocumentStatus 
                from PAEZ_t_NonsaleApplyEntry ne
                inner join PAEZ_t_NonsaleApply n on n.FID=ne.FID and FBillTypeID='602d00ea7fe711'
                inner join PAEZ_SaleoutSumEntry_LK selk on selk.FSId=ne.FEntryId and selk.FSTableName='PAEZ_t_NonsaleApplyEntry'
                left join T_STK_STKTRANSFERAPPENTRY_LK talk on talk.FSID=selk.FEntryID and talk.FSTableName='PAEZ_SaleoutSumEntry'
                left join T_STK_STKTRANSFERINENTRY_LK tilk on tilk.FSID=talk.FEntryID and tilk.FSTableName='T_STK_STKTRANSFERAPPENTRY'
                left join T_STK_STKTRANSFERINENTRY tie on tie.FENTRYID=tilk.FENTRYID
                left join T_STK_STKTRANSFERIN ti on ti.FID=tie.FID
            )t where FDocumentStatus<>'C'
        )ne
        group by ne.FMaterialID
    ) nai on nai.FMaterialId=inv.FMaterialId
    left join (
        -- 减去售后申请单未出库的占用数量, 非借用都走售后流程
        select ne.FMaterialID, sum(ne.FQTY) FQty from (
            select distinct * from (
                select  ne.FEntryID, ne.FMaterialID, ne.FQty, isnull(md.FDocumentStatus, '') FDocumentStatus
                from PAEZ_t_NonsaleApplyEntry ne
                inner join PAEZ_t_NonsaleApply n on n.FID=ne.FID and FBillTypeID<>'602d00ea7fe711' --='602d01347fe889'
                inner join PAEZ_SaleoutSumEntry_LK selk on selk.FSId=ne.FEntryId and selk.FSTableName='PAEZ_t_NonsaleApplyEntry'
                left join T_STK_OUTSTOCKAPPLYENTRY_LK oalk on oalk.FSID=selk.FEntryID and oalk.FSTableName='PAEZ_SaleoutSumEntry'
                left join T_STK_MISDELIVERYENTRY_LK mdlk on mdlk.FSID=oalk.FEntryID and mdlk.FSTableName='T_STK_OUTSTOCKAPPLYENTRY'
                left join T_STK_MISDELIVERYENTRY mde on mde.FENTRYID=mdlk.FENTRYID
                left join T_STK_MISDELIVERY md on md.FID=mde.FID
            )t where FDocumentStatus<>'C'
        )ne
        group by ne.FMaterialID
    ) asi on inv.FMaterialId=asi.FMaterialId
    left join (
        -- 将库存减去未出库的提货联的数量
        select le.FMaterialID, sum(le.FQty) FQty from (
            select distinct le.FEntryID, le.FMATERIALID, le.FQTY from PAEZ_t_Lading l
            inner join PAEZ_t_LadingEntry le on le.FID=l.FID and F_PAEZ_YWZF=0
            left join PAEZ_SaleoutSumEntry_LK selk on selk.FSId=le.FEntryId and selk.FSTableName='PAEZ_t_LadingEntry'
            left join T_SAL_DELIVERYNOTICEENTRY_LK delk on delk.FSId=selk.FEntryID and delk.FSTableName='PAEZ_SaleoutSumEntry'
            left join T_SAL_OUTSTOCKENTRY_LK oselk on oselk.FSId=delk.FENTRYID and oselk.FSTableName='T_SAL_DELIVERYNOTICEENTRY'
            left join T_SAL_OUTSTOCKENTRY ose on ose.FEntryId=oselk.FEntryId
            left join T_SAL_OUTSTOCK os on os.FID=ose.FID
            where isnull(os.FDocumentStatus, '')<>'C' and l.FRepeatInvoice=0
        )le
        group by le.FMaterialID
    ) ldi on inv.FMaterialId=ldi.FMaterialId
    where inv.FNumber like('%'+@mtl_number+'%')
)
    


-- select * from dbo.fun_czly_GetMtlStkOcupy('')