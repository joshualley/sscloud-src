USE [AIS202011SHYLQX]
GO
/****** Object:  StoredProcedure [dbo].[proc_czly_QueueResult]    Script Date: 2023/2/21 18:12:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- 排队结果查询
ALTER proc [dbo].[proc_czly_QueueResult](
    @is_limit int=1, -- 是否限制额度
    @bill_type varchar(10),
    @billno varchar(55),
    @mtl_number varchar(55),
    @cust_number varchar(55),
    @saler_name varchar(55)
) as
begin
set nocount on
-- 销售订单、出库申请、返工生产订单、订配包物料排队

-- declare @t datetime=getdate()

set @bill_type=isnull(@bill_type, '')
set @billno=isnull(@billno, '')
set @mtl_number=isnull(@mtl_number, '')
set @cust_number=isnull(@cust_number, '')
set @saler_name=isnull(@saler_name, '')

CREATE TABLE #t_queue_temp(
    FQSeq INT PRIMARY KEY IDENTITY(1, 1),
    FBillType INT, -- 1: 销售订单; 2: 生产订单; 3：借用/售后
    FID INT,
    FEntryId INT,
    FSeq INT,
    FMaterialId BIGINT,
    FQty DECIMAL(18,2),
    F_PAEZ_KyQty DECIMAL(18,2) DEFAULT(0), -- 可用量
    FDate DATETIME,
    F_PAEZ_Priority INT, -- 优先级 
    F_PAEZ_ChuPiao INT,  -- 出票方式：整单 0、流水 1
    FRetainQty DECIMAL(18,2) DEFAULT(0), -- 保留数
    FIsAllowDelv INT DEFAULT(0),    -- 是否允许发货
    FCustId BIGINT DEFAULT(0),
    F_PAEZ_FMkt VARCHAR(55) DEFAULT(NULL), --订单类型
    F_PAEZ_DiZhi VARCHAR(55) DEFAULT(NULL), -- 地址类型
    FSplitBillNum INT DEFAULT(0), -- 分单号
    FPickQty DECIMAL(18,2) DEFAULT(0), --累计提货数量
    FTerminateStatus VARCHAR(30) DEFAULT('A'),
    F_PAEZ_FHSX INT -- 发货顺序: 0,1,2    0最优先
)

DECLARE @use_org_id INT=100008

------------------------------------ 2、从三类单据获取数据 ------------------------------------
-- 获取符合条件的销售订单行：已审核、行未关闭、未推发货联
INSERT INTO #t_queue_temp(FBillType,FID,FEntryId,FSeq,FMaterialId,FQty,F_PAEZ_KyQty,FDate,F_PAEZ_Priority,F_PAEZ_ChuPiao
    ,FCustId,F_PAEZ_FMkt,F_PAEZ_DiZhi,FPickQty,FTerminateStatus,F_PAEZ_FHSX)
SELECT 1,
    o.FID,oe.FEntryId,oe.FSeq,oe.FMaterialId,oe.FQty,oe.F_PAEZ_KyQty,o.F_PAEZ_QueueDt,o.F_PAEZ_Priority,o.F_PAEZ_ChuPiao
    ,o.FCustId,o.F_PAEZ_FMkt,o.F_PAEZ_DiZhi,oe.F_PAEZ_PickQty,oe.FMrpTerminateStatus,o.F_PAEZ_FHSX
FROM T_SAL_ORDER o
INNER JOIN T_BD_CUSTOMER c ON o.FCustID=c.FCustID AND c.F_PAEZ_Pause=0
INNER JOIN T_SAL_ORDERENTRY oe ON o.FID=oe.FID 
INNER JOIN T_BD_MATERIAL m ON m.FMaterialId=oe.FMaterialId
WHERE o.FDocumentStatus='C' AND oe.FMRPCLOSESTATUS='A' and o.FCloseStatus='A' 
-- 不为K0订单
AND o.F_PAEZ_FMkt<>'K0'
-- 行终止但具有保留分配数标记的不过滤
AND (oe.FMrpTerminateStatus='A' or (oe.FMrpTerminateStatus='B' and o.F_PAEZ_TerminatedRetain=1))
-- AND dbo.fun_czty_CustAR_MP3(o.FCustID)<=0 -- 过滤掉赊销客户
AND (select FBackM from dbo.fun_czty_ChkSalCust(o.FCustID,'sx',0))='OK' -- 过滤掉赊销客户
AND oe.F_PAEZ_PickQty<oe.FQty -- 累计提货数量小于订单数量
AND NOT (m.F_CZ_YLLB='5f76c02bc718ec' AND isnull(c.F_PAEZ_3DQDate, GETDATE())<=GETDATE()) -- 过滤掉 物料为三类, 并且客户的生成许可日期过期的
AND o.FSaleOrgId=@use_org_id --组织为上手金钟
-- 先对订单数据进行一次排序
ORDER BY o.F_PAEZ_Priority DESC, o.F_PAEZ_FHSX ASC, o.F_PAEZ_QueueDt ASC, oe.FQty-oe.F_PAEZ_KyQty DESC

-- 生产订单需要与生产用料清单联表，然后计算每个配包的库存是否满足发货，这里将生产订单的一行当作一单
INSERT INTO #t_queue_temp(FBillType,FID,FEntryId,FSeq,FMaterialId,FQty,FPickQty,FDate,F_PAEZ_Priority,F_PAEZ_ChuPiao,F_PAEZ_FHSX)
SELECT 2,
    moe.FEntryId,pbe.FEntryId,pbe.FSeq,pbe.FMaterialId,pbe.FMustQty,pbeq.FSelPickedQty,mo.FApproveDate,mo.F_PAEZ_Priority,
    0,  -- 用料清单视为整单
    0  -- 发货顺序为 0
FROM T_PRD_MO mo
INNER JOIN T_PRD_MOENTRY moe ON moe.FID=mo.FID 
INNER JOIN T_PRD_MOENTRY_Q moeq ON moeq.FEntryID=moe.FEntryID AND moeq.FPickMtrlStatus=1 --未领料 
INNER JOIN T_PRD_MOENTRY_A moea ON moea.FEntryID=moe.FEntryID AND moea.FStatus IN ('1','2','3','4')
INNER JOIN T_BD_MATERIAL m ON m.FMaterialID=moe.FMaterialId
-- 关联生产用料清单
inner join T_PRD_PPBOM pb on pb.FMOEntryID=moe.FEntryID
inner join T_PRD_PPBOMENTRY pbe on pbe.FID=pb.FID
inner join T_PRD_PPBOMENTRY_Q pbeq on pbeq.FENTRYID=pbe.FENTRYID
WHERE mo.FDocumentStatus='C'
AND m.FMATERIALGROUP=351757 --物料分组为订配包
AND pbeq.FSelPickedQty < pbe.FMustQty --领料数量小于应发数量
-- AND NOT EXISTS(SELECT FSRCBIZBILLNO FROM T_PRD_PICKMTRLDATA_A WHERE FSRCBIZBILLTYPE=@formid AND FSrcBillNo=mo.FBILLNO)
AND mo.FPrdOrgId=@use_org_id --发料组织为上手


select * into #NonsaleApplyEntry
from (
    select oe.FID,oe.FEntryId,oe.FSeq,oe.FMaterialId,oe.FQty,oe.FCLOSESTATUS,
        isnull(lk.FEntryID, 0) FLkId from PAEZ_t_NonsaleApplyEntry oe
    left join PAEZ_SaleoutSumEntry_LK lk on lk.FSId=oe.FEntryID and FSTableName='PAEZ_t_NonsaleApplyEntry'
)t where FLkId=0 and FCLOSESTATUS='A'

-- 借用、售后申请
INSERT INTO #t_queue_temp(FBillType,FID,FEntryId,FSeq,FMaterialId,FQty,FDate,F_PAEZ_Priority,F_PAEZ_ChuPiao,F_PAEZ_FHSX)
SELECT 3,
    o.FID,oe.FEntryId,oe.FSeq,oe.FMaterialId,oe.FQty,o.FApproveDate,o.F_PAEZ_Priority, 
    0, -- 全部视为整单
    1  -- 发货顺序为 1
    -- CASE o.FBillTypeID 
    --     WHEN '602d00ea7fe711' THEN 0  -- 借用：整单
    --     WHEN '602d01347fe889' THEN 1  -- 售后：流水
    --     ELSE 0 END -- 其他：整单
FROM PAEZ_t_NonsaleApply o
-- INNER JOIN T_BD_CUSTOMER c ON o.FCustomerID=c.FCustID AND c.F_PAEZ_Pause=0
INNER JOIN #NonsaleApplyEntry oe ON o.FID=oe.FID 
WHERE o.FDocumentStatus='C' AND oe.FCLOSESTATUS='A'
-- AND NOT EXISTS(SELECT FEntryId FROM PAEZ_SaleoutSumEntry_LK WHERE FSTableName='PAEZ_t_NonsaleApplyEntry' AND FSId=oe.FEntryID)
AND o.FSaleOrgId=@use_org_id --申请组织为上手

drop table #NonsaleApplyEntry
------------------------------------ 2、从三类单据获取数据 ------------------------------------

------------------------------------   3、对数据进行排队   ------------------------------------
CREATE TABLE #t_queue(
    FQSeq INT PRIMARY KEY IDENTITY(1, 1),
    FBillType INT, -- 1: 销售订单; 2: 生产订单; 3：借用/售后
    FID INT,
    FEntryId INT,
    FSeq INT,
    FMaterialId BIGINT,
    FQty DECIMAL(18,2),
    F_PAEZ_KyQty DECIMAL(18,2) DEFAULT(0), -- 可用量
    FDate DATETIME,
    F_PAEZ_Priority INT, -- 优先级 
    F_PAEZ_ChuPiao INT,  -- 出票方式：整单 0、流水 1
    FRetainQty DECIMAL(18,2) DEFAULT(0), -- 保留数
    FIsAllowDelv INT DEFAULT(0),    -- 是否允许发货
    FCustId BIGINT DEFAULT(0),
    F_PAEZ_FMkt VARCHAR(55) DEFAULT(NULL), --订单类型
    F_PAEZ_DiZhi VARCHAR(55) DEFAULT(NULL), -- 地址类型
    FSplitBillNum INT DEFAULT(0), -- 分单号
    FPickQty DECIMAL(18,2) DEFAULT(0), --累计提货数量
    FTerminateStatus VARCHAR(30) DEFAULT('A'),
    F_PAEZ_FHSX INT, -- 发货顺序: 0,1,2    0最优先
    FReason VARCHAR(255)
)
-- 优先级依据：优先级[整数]大的{优先级相同时：时间早的>整单>流水出票}
INSERT INTO #t_queue(
    FBillType,FID,FEntryId,FSeq,FMaterialId,FQty,F_PAEZ_KyQty,FDate,F_PAEZ_Priority,F_PAEZ_ChuPiao,
    FRetainQty,FIsAllowDelv,FCustId,F_PAEZ_FMkt,F_PAEZ_DiZhi,FSplitBillNum,FPickQty,FTerminateStatus,F_PAEZ_FHSX,
    FReason
)
SELECT 
    FBillType,FID,FEntryId,FSeq,FMaterialId,FQty,F_PAEZ_KyQty,FDate,F_PAEZ_Priority,F_PAEZ_ChuPiao,
    FRetainQty,FIsAllowDelv,FCustId,F_PAEZ_FMkt,F_PAEZ_DiZhi,FSplitBillNum,FPickQty,FTerminateStatus,F_PAEZ_FHSX,
    ''
FROM #t_queue_temp
ORDER BY F_PAEZ_Priority DESC, F_PAEZ_FHSX ASC, FDate ASC, F_PAEZ_KyQty-FQty DESC

CREATE INDEX Quene_MId ON #t_queue(FMaterialId)
------------------------------------   3、对数据进行排队   ------------------------------------

-- print('1.------------- ' + convert(varchar(20), datediff(second, @t, getdate())))

------------------------------------ 4、获取物料的即时库存 ------------------------------------
select i.FMaterialId, FleftStkQty, FleftStkQty FRemainStkQty
into #t_inventory
from dbo.fun_czly_GetMtlStkOcupy('') i
inner join (
    select distinct FMaterialId from #t_queue
) q on q.FMaterialId=i.FMaterialId

CREATE INDEX Inv_MId ON #t_inventory(FMaterialId)
------------------------------------ 4、获取物料的即时库存 ------------------------------------
-- print('2.------------- ' + convert(varchar(20), datediff(second, @t, getdate())))
---------------------------------- 5、分配保留数及设置发货标记 ----------------------------------
-- 1.根据顺序计算单据是否允许发货, 并更新订单中的保留数
DECLARE @max_seq INT=(SELECT COUNT(*) FROM #t_queue)
        , @i INT=0
        , @fid INT

WHILE @i < @max_seq
BEGIN
    SET @i += 1
    -- 计算是否允许发货，并得到保留数
    UPDATE o SET 
        o.FIsAllowDelv=CASE WHEN i.FRemainStkQty>=o.FQty-o.FPickQty THEN 1 ELSE 0 END,
        o.FRetainQty=CASE
            WHEN i.FRemainStkQty <= 0 THEN 0
            WHEN i.FRemainStkQty>=o.FQty-o.FPickQty THEN o.FQty-o.FPickQty 
            ELSE i.FRemainStkQty END
    FROM #t_queue o
    INNER JOIN #t_inventory i ON o.FMaterialId=i.FMaterialId
    WHERE o.FQSeq=@i
    -- 更新剩余库存
    UPDATE i SET 
        i.FRemainStkQty=CASE 
            WHEN i.FRemainStkQty>=o.FQty-o.FPickQty THEN i.FRemainStkQty-o.FQty+o.FPickQty 
            ELSE 0 END
    FROM #t_inventory i
    INNER JOIN #t_queue o ON o.FMaterialId=i.FMaterialId AND o.FQSeq=@i
END

-- 2.检查整单订单是否全部可以发货
SELECT FID, FBillType,
    (SELECT COUNT(FQSeq) FROM #t_queue q WHERE q.FID=t.FID and q.FBillType=t.FBillType and q.FTerminateStatus='A') FMaxSeq, 
    (SELECT COUNT(FQSeq) FROM #t_queue q WHERE q.FID=t.FID and q.FBillType=t.FBillType AND q.FIsAllowDelv=1 and q.FTerminateStatus='A') FCnt, 
    0 FIsAllow
INTO #t_delv_check 
FROM #t_queue t WHERE F_PAEZ_ChuPiao=0
GROUP BY FID, FBillType

UPDATE t SET FIsAllow=1 FROM #t_delv_check t WHERE FMaxSeq=FCnt

UPDATE q SET FIsAllowDelv=t.FIsAllow, 
    FReason=case t.FIsAllow when 1 then '' else '不符合发货条件' end
FROM #t_queue q 
INNER JOIN #t_delv_check t ON t.FID=q.FID and q.FBillType=t.FBillType
-- 将行业务终止的都设置为不允许
update #t_queue set FIsAllowDelv=0, FReason='行业务终止' where FTerminateStatus='B'

DROP TABLE #t_delv_check

-- 3.流水情况下，保留数大于0，即允许发货, 发货数量为保留数
UPDATE #t_queue SET FIsAllowDelv=1 WHERE FRetainQty>0 and F_PAEZ_ChuPiao=1

-- ！！！此处为拷贝的其他人的逻辑
-- ！！！此处为拷贝的其他人的逻辑
-- ！！！此处为拷贝的其他人的逻辑
-->--------------------------------- 5.1、过滤掉订单中信用额度不够的客户订单 ---------------------------------<--
declare @FSchDate datetime=GETDATE()
declare @MyTableType as table (销售员 varchar(20),客户名称 varchar(50),账期 varchar(20),期初欠款 decimal(18,2),	[1月销售] decimal(18,2),[1月回笼] decimal(18,2),	[2月销售]decimal(18,2),	[2月回笼]decimal(18,2),	[3月销售]decimal(18,2),	[3月回笼]decimal(18,2),	[4月销售]decimal(18,2),	[4月回笼]decimal(18,2),	[5月销售]decimal(18,2),	[5月回笼]decimal(18,2),	[6月销售]decimal(18,2),	[6月回笼]decimal(18,2),	[7月销售]decimal(18,2),	[7月回笼]decimal(18,2),	[8月销售]decimal(18,2),	[8月回笼]decimal(18,2),	[9月销售]decimal(18,2),	[9月回笼]decimal(18,2),	[10月销售]decimal(18,2),	[10月回笼]decimal(18,2),	[11月销售]decimal(18,2),	[11月回笼]decimal(18,2),	[12月销售]decimal(18,2),	[12月回笼]decimal(18,2),	FCustomerID	varchar(20),累计销售 decimal(18,2),	累计回笼 decimal(18,2),	序号 int,	累计欠款 decimal(18,2),	累计回笼率 varchar(20))
Insert into @MyTableType exec proc_zz_cxxsyhl @FSchDate 
SELECT FCustomerID,convert(varchar(8),'') FGroupNo,累计欠款 ljqk,convert(numeric(18,2),0.00) xyed,convert(numeric(18,2),0.00) wzxje,convert(numeric(18,2),0.00) kyje into #hzbb FROM @MyTableType where FCustomerID is not null
--信用额度
update #hzbb set xyed = a.信用额度 from (
    select FCUSTID,isnull(f_paez_xyed+f_paez_zjxyed,0) 信用额度 from T_BD_CUSTOMER) a where a.FCUSTID=FCustomerID

insert into #hzbb(FCustomerID,ljqk,xyed,wzxje,kyje) 
    select FCUSTID,0.00 ljqk,isnull(f_paez_xyed+f_paez_zjxyed,0) xyed,0.00 wxzje,0.00 kyje from T_BD_CUSTOMER where FUSEORGID=100008 AND f_paez_xyed+f_paez_zjxyed<>0 AND not exists (select FCustomerID from #hzbb WHERE #hzbb.FCustomerID=T_BD_CUSTOMER.FCUSTID)
--关联客户
SELECT PCK.FBILLNO,PCKE.F_PAEZ_KH,ISNULL(HZ.xyed,0) xyed,ISNULL(HZ.ljqk,0) LJQK,ISNULL(HZ.wzxje,0) wzxje INTO #GXK FROM PAEZ_CHENG_KHXYEDGL PCK 
    INNER JOIN PAEZ_CHENG_KHXYEDGLENTRY PCKE ON PCK.FID=PCKE.FID 
    LEFT JOIN #hzbb HZ ON PCKE.F_PAEZ_KH=HZ.FCustomerID
    WHERE PCK.FDOCUMENTSTATUS='C'

UPDATE #GXK SET XYED=A.XYED,LJQK=A.LJQK,WZXJE=A.WZXJE FROM (SELECT FBILLNO,SUM(XYED) XYED,SUM(LJQK) LJQK,SUM(WZXJE) WZXJE FROM #GXK GROUP BY FBILLNO) A WHERE A.FBILLNO=#GXK.FBILLNO

UPDATE #hzbb SET XYED=A.XYED,LJQK=A.LJQK,WZXJE=A.WZXJE,FGroupNo=a.FBILLNO FROM #GXK A WHERE A.F_PAEZ_KH=#hzbb.FCustomerID

insert into #hzbb(FCustomerID,ljqk,xyed,wzxje,kyje) 
    select F_PAEZ_KH,ljqk,xyed,wzxje,0.00 kyje from #GXK where not exists (select FCustomerID from #hzbb WHERE #hzbb.FCustomerID=#GXK.F_PAEZ_KH)

update #hzbb set kyje=xyed-ljqk
update #hzbb set FGroupNo= '' where FGroupNo is null
-- 共用额度的客户在一组
select FCustomerID FCustId, kyje FBalance, FGroupNo into #cust_balance from #hzbb

drop table #GXK
drop table #hzbb
-->--------------------------------- 5.1、过滤掉订单中信用额度不够的客户订单 ---------------------------------<--

-->--------------------------------- 5.2、过滤掉订单中信用额度不够的客户订单 ---------------------------------<--

-- 获取订单中的客户, 客户信用额度(存在多客户共用额度的情况), 金额(整单订单金额进行合并)
select q.FQSeq, q.FID, q.FCustId,
    case q.F_PAEZ_ChuPiao when 1 then q.FEntryId else 0 end FEntryId,
    oef.FTaxPrice*(q.FQty-q.FPickQty)*(1-oef.FDiscountRate/100) FAmount,
    isnull(b.FBalance, 0) FBalance,
    b.FGroupNo
into #cust_tag_t
from #t_queue q
left join #cust_balance b on q.FCustId=b.FCustId
inner join T_SAL_ORDERENTRY_F oef on oef.FID=q.FID and oef.FEntryId=q.FEntryId
where q.FIsAllowDelv=1
order by q.FQSeq

-- 计算整单订单的可排队发货的金额
select FID, sum(FAmount) FBillAmount into #t_sum
from #cust_tag_t where FEntryId=0 group by FID

update t set t.FAmount=s.FBillAmount
from #cust_tag_t t 
inner join #t_sum s on s.FID=t.FID
-- 将整单订单合并为一行数据
select distinct FID into #oids from #cust_tag_t where FEntryId=0
declare @oid_t bigint
declare @mseq bigint
while EXISTS(select FID from #oids)
begin
    select top 1 @oid_t=FID from #oids
    delete from #oids where FID=@oid_t
    -- 获取最小序号
    select @mseq = MIN(FQSeq) from #cust_tag_t where FID=@oid_t
    delete from #cust_tag_t where FID=@oid_t and FQSeq>@mseq
end
drop table #oids

select row_number() over(order by FQSeq) FQSeq, 
    FID, FEntryId, FCustId, FAmount, FBalance, FGroupNo, 0 FIsAllow
into #cust_tag
from #cust_tag_t order by FQSeq

-- 循环校验销售订单中客户的信用额度是否足够
declare @oid int, 
    @eid int, 
    @cid int,
    @len int=(select max(FQSeq) from #cust_tag), 
    @oseq int=0, 
    @amt decimal(23,6), 
    @gp varchar(10)
while @oseq < @len
begin
    set @oseq += 1
    select @oid=FID, @eid=FEntryId, @cid=FCustId from #cust_tag where FQSeq=@oseq

    -- 当金额小于客户信用余额时，标记为允许排队
    set @amt = 0
    set @gp = ''
    select @amt=FAmount, @gp=FGroupNo from #cust_tag where FID=@oid and @eid=FEntryId and FAmount<=FBalance
    if @amt <> 0
    begin
        update #cust_tag set FIsAllow=1 where FID=@oid and FEntryId=@eid
        -- 标记后，扣减掉该客户的余额
        -- select @amt=FAmount from #cust_tag where FID=@id and @FEntryId=@eid 
        if @gp <> ''
        begin
            update #cust_tag set FBalance=FBalance-@amt where FGroupNo=@gp
        end
        else
        begin
            update #cust_tag set FBalance=FBalance-@amt where FCustId=@cid
        end
    end
end

-- 未标记的整单数据 设置为不允许发货
update q set FIsAllowDelv=0, FReason='客户信用额度不足' from #t_queue q
inner join #cust_tag b on q.FID=b.FID
where b.FEntryId=0 and b.FIsAllow=0 and q.FBillType=1
-- 未标记的流水数据 设置为不允许发货
update q set FIsAllowDelv=0, FReason='客户信用额度不足' from #t_queue q
inner join #cust_tag b on q.FID=b.FID and q.FEntryId=b.FEntryId
where b.FIsAllow=0 and q.FBillType=1

drop table #cust_tag_t
drop table #t_sum
drop table #cust_tag
------------------------------------ 5.2、过滤掉订单中信用额度不够的客户订单 ------------------------------------

---------------------------------- 5、分配保留数及设置发货标记 ----------------------------------

-- print('3.------------- ' + convert(varchar(20), datediff(second, @t, getdate())))

select q.FQSeq,
    convert(varchar(20), case q.FBillType 
        when 1 then '销售订单' 
        when 2 then '生产用料清单'
        when 3 then '借用-售后申请单'
        end) FBType,
    q.FBillNo, q.FCustNumber, q.FCustName, q.FSalerName, q.FSeq, q.FMaterialId, m.FNumber FMtlNumber, ml.FName FMtlName,
    isnull(i.FLeftStkQty, 0)FLeftStkQty, q.FQty-q.FPickQty as FQty, q.FRetainQty, isnull(i.FRemainStkQty, 0)FRemainStkQty, q.FIsAllowDelv,
    q.FReason, cb.FBalance
from (
    select o.FBILLNO,  c.FNumber FCustNumber, cl.FName FCustName, isnull(sml.FName, '') FSalerName, q.* 
    from #t_queue q 
    inner join T_SAL_ORDER o on o.FID=q.FID and q.FBillType=1
    inner join T_BD_Customer c on c.FCustId=o.FCustId
    inner join T_BD_Customer_L cl on cl.FCustId=c.FCustId
    left join V_BD_SALESMAN_L sml on sml.FID=o.FSalerId
    union all
    select pb.FBILLNO, '' FCustNumber, '' FCustName, '' FSalerName, q.* 
    from #t_queue q 
    inner join T_PRD_PPBOMEntry pbe on pbe.FEntryId=q.FEntryId and q.FBillType=2
    inner join T_PRD_PPBOM pb on pb.FID=pbe.FID
    union all
    select na.FBILLNO, c.FNumber FCustNumber, cl.FName FCustName, sml.FName FSalerName, q.* 
    from #t_queue q 
    inner join PAEZ_t_NonsaleApply na on na.FID=q.FID and q.FBillType=3
    inner join T_BD_Customer c on c.FCustId=na.FCustomerID
    inner join T_BD_Customer_L cl on cl.FCustId=c.FCustId
    left join V_BD_SALESMAN_L sml on sml.FID=na.FSalesManID
)q
left join #t_inventory i on i.FMaterialId=q.FMaterialId
left join #cust_balance cb on cb.FCustId=q.FCustId
inner join T_BD_MATERIAL m on m.FMaterialId=q.FMATERIALID 
inner join T_BD_MATERIAL_L ml on ml.FMaterialId=m.FMATERIALID 
where (@bill_type='' or q.FBillType=@bill_type)
and (@billno='' or q.FBILLNO like @billno+'%')
and (@mtl_number='' or m.FNumber like @mtl_number+'%')
and (@cust_number='' or q.FCustNumber like @cust_number+'%')
and (@saler_name='' or q.FSalerName like @saler_name+'%')
order by q.FQSeq

drop table #cust_balance
DROP TABLE #t_queue_temp
DROP TABLE #t_queue
DROP TABLE #t_inventory

end 

-- exec proc_czly_QueueResult @is_limit=1, @bill_type='', @billno='', @mtl_number='', @cust_number='', @saler_name=''
/*
exec proc_czly_QueueResult @bill_type='#FQueueBillType#', @billno='#FBillNO#', @mtl_number='#FNumber#',
    @cust_number='#FCustNo#', @saler_name='#FSalerName#'
*/