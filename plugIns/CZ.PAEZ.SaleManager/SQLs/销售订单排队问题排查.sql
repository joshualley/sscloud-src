alter proc proc_czly_QueueProblemAnalysis (
    @billno varchar(55),
    @mtlno varchar(55),
    @custno varchar(55)
)
as
begin

if @billno<>'' or @custno<>'' or @mtlno<>''
begin
    SELECT o.FBillNo as 订单编号, oe.FSeq as 行号, 
        case when o.FDocumentStatus='C' then '正常' else '单据未审核' end as 单据状态,
        case when o.FCloseStatus='A' then '正常' else '单据已关闭' end as 关闭状态,
        case when oe.FMRPCLOSESTATUS='A' then '正常' else '行已关闭' end as 行关闭状态,
        case when o.F_PAEZ_FMkt<>'K0' then '正常' else 'K0订单不参与排队' end as 订单类型,
        case when (oe.FMrpTerminateStatus='A' or (oe.FMrpTerminateStatus='B' and o.F_PAEZ_TerminatedRetain=1)) 
            then '正常' else '行终止' end as 业务终止,
        c.FNumber as 客户编码, cl.FName as 客户名称,
        case when c.F_PAEZ_Pause=0 then '正常' else '客户暂停排队' end as 暂停排队,
        case when (select FBackM from dbo.fun_czty_ChkSalCust(o.FCustID,'sx',0))='OK' 
            then '正常' else '赊销客户' end as 是否赊销,
        case when NOT (m.F_CZ_YLLB='5f76c02bc718ec' AND isnull(c.F_PAEZ_3DQDate, GETDATE())<=GETDATE()) 
            then '正常' else '客户生成许可日期过期' end as 客户许可日期
    FROM T_SAL_ORDER o
    INNER JOIN T_BD_CUSTOMER c ON o.FCustID=c.FCustID 
    INNER JOIN T_BD_CUSTOMER_L cl ON cl.FCustID=c.FCustID
    INNER JOIN T_SAL_ORDERENTRY oe ON o.FID=oe.FID 
    INNER JOIN T_BD_MATERIAL m ON m.FMaterialId=oe.FMaterialId
    WHERE oe.F_PAEZ_PickQty<oe.FQty -- 累计提货数量小于订单数量
    AND o.FSaleOrgId=100008 --组织为上手金钟
    AND (
        o.FBillNO = @billno
        OR c.FNumber = @custno
        OR m.FNumber = @mtlno
    )
end
else
begin
    SELECT o.FBillNo as 订单编号, oe.FSeq as 行号, 
        case when o.FDocumentStatus='C' then '正常' else '单据未审核' end as 单据状态,
        case when o.FCloseStatus='A' then '正常' else '单据已关闭' end as 关闭状态,
        case when oe.FMRPCLOSESTATUS='A' then '正常' else '行已关闭' end as 行关闭状态,
        case when o.F_PAEZ_FMkt<>'K0' then '正常' else 'K0订单不参与排队' end as 订单类型,
        case when (oe.FMrpTerminateStatus='A' or (oe.FMrpTerminateStatus='B' and o.F_PAEZ_TerminatedRetain=1)) 
            then '正常' else '行终止' end as 业务终止,
        c.FNumber as 客户编码, cl.FName as 客户名称,
        case when c.F_PAEZ_Pause=0 then '正常' else '客户暂停排队' end as 暂停排队,
        case when (select FBackM from dbo.fun_czty_ChkSalCust(o.FCustID,'sx',0))='OK' 
            then '正常' else '赊销客户' end as 是否赊销,
        case when NOT (m.F_CZ_YLLB='5f76c02bc718ec' AND isnull(c.F_PAEZ_3DQDate, GETDATE())<=GETDATE()) 
            then '正常' else '客户生成许可日期过期' end as 客户许可日期
    FROM T_SAL_ORDER o
    INNER JOIN T_BD_CUSTOMER c ON o.FCustID=c.FCustID 
    INNER JOIN T_BD_CUSTOMER_L cl ON cl.FCustID=c.FCustID
    INNER JOIN T_SAL_ORDERENTRY oe ON o.FID=oe.FID 
    INNER JOIN T_BD_MATERIAL m ON m.FMaterialId=oe.FMaterialId
    WHERE oe.F_PAEZ_PickQty<oe.FQty -- 累计提货数量小于订单数量
    AND o.FSaleOrgId=100008 --组织为上手金钟
    and (
        o.FCloseStatus<>'A'
        or oe.FMRPCLOSESTATUS<>'A'
        or o.F_PAEZ_FMkt='K0'
        or not (oe.FMrpTerminateStatus='A' or (oe.FMrpTerminateStatus='B' and o.F_PAEZ_TerminatedRetain=1)) 
        or c.F_PAEZ_Pause=1
        or (select FBackM from dbo.fun_czty_ChkSalCust(o.FCustID,'sx',0))<>'OK'
        or (m.F_CZ_YLLB='5f76c02bc718ec' AND isnull(c.F_PAEZ_3DQDate, GETDATE())<=GETDATE()) 
    )
end

end


-- exec proc_czly_QueueProblemAnalysis @billno='35000001220010A0', @custno='', @mtlno=''