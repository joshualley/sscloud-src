-- 物料库存占用查询
alter proc proc_czly_MtlStkOcupyQuery(
    @mtl_number varchar(100),
    @query_type varchar(20)
) as
begin 
set nocount on

select 
    FMaterialId, FNumber 物料编码,
    FQtyInv 即时库存, FQtyLock 锁库数量, 
    FQtyProd 领料占用, FLdQty 提货占用, 
    FNaQty 借用占用, FAsQty 售后占用, 
    FleftStkQty 可用库存
from dbo.fun_czly_GetMtlStkOcupy(@mtl_number)
where (@query_type='2' OR FleftStkQty<0)

end

-- exec proc_czly_MtlStkOcupyQuery @mtl_number='#FMaterialId#', @query_type='#FQType#'