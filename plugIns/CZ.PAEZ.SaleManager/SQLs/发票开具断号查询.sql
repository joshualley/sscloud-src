-- ��Ʊ���߶ϺŲ�ѯ
alter proc proc_czly_InvoiceCutOffQuery(
    @date datetime
)
as
begin
set nocount on

if @date is null set @date=GETDATE()

select distinct FID, FInvoiceDate, FBillNo, FNumber FCustNo, FName FCustName, FInvoice, FInvoiceR, FInvoiceNo, FIvNumber
from (
    select sc.FID, sc.FInvoiceDate, l.FBillNo, c.FNumber, cl.FName, l.FInvoice, '' FInvoiceR, sc.FInvoiceNo, gi.FIvNumber
    from PAEZ_t_Lading l
    inner join PAEZ_t_LadingEntry le on le.FID=l.FID
    inner join T_BD_Customer c on c.FCustId=l.FCustomerID
    inner join T_BD_Customer_L cl on cl.FCustId=c.FCustId
    inner join T_IV_SALESICENTRY_LK scelk on scelk.FSId=le.FEntryId and scelk.FSTableName='PAEZ_t_LadingEntry'
    inner join T_IV_SALESICENTRY sce on sce.FEntryId=scelk.FEntryId
    inner join T_IV_SALESIC sc on sc.FID=sce.FID
    inner join T_IV_GTINVOICEENTRY_LK ielk on ielk.FSId=sce.FEntryId and ielk.FSTableName='T_IV_SALESICENTRY'
    inner join T_IV_GTINVOICEENTRY gie on gie.FEntryId=ielk.FEntryId
    inner join T_IV_GTINVOICE gi on gi.FID=gie.FID
    union all 
    select sc.FID, sc.FInvoiceDate, l.FBillNo, c.FNumber, cl.FName, l.FInvoice, r.FBillNo FInvoiceR, sc.FInvoiceNo, gi.FIvNumber
    from PAEZ_t_Lading l
    inner join PAEZ_t_LadingEntry le on le.FID=l.FID
    inner join T_BD_Customer c on c.FCustId=l.FCustomerID
    inner join T_BD_Customer_L cl on cl.FCustId=c.FCustId
    inner join t_AR_receivableEntry_LK relk on relk.FSId=le.FEntryId and relk.FSTableName='PAEZ_t_LadingEntry'
    inner join t_AR_receivableEntry re on re.FEntryId=relk.FEntryId
    inner join t_AR_receivable r on r.FID=re.FID
    inner join T_IV_SALESICENTRY_LK scelk on scelk.FSId=relk.FEntryId and scelk.FSTableName='t_AR_receivableEntry'
    inner join T_IV_SALESICENTRY sce on sce.FEntryId=scelk.FEntryId
    inner join T_IV_SALESIC sc on sc.FID=sce.FID
    inner join T_IV_GTINVOICEENTRY_LK ielk on ielk.FSId=sce.FEntryId and ielk.FSTableName='T_IV_SALESICENTRY'
    inner join T_IV_GTINVOICEENTRY gie on gie.FEntryId=ielk.FEntryId
    inner join T_IV_GTINVOICE gi on gi.FID=gie.FID
)t where (FInvoice<>FInvoiceNo or FInvoice<>FIvNumber or FInvoiceNo<>FIvNumber)
and year(FInvoiceDate)=year(@date) and month(FInvoiceDate)=month(@date)

end

-- exec proc_czly_InvoiceCutOffQuery @date=''