using System;
using Kingdee.BOS;
using Kingdee.BOS.App;
using Kingdee.BOS.Contracts;
using Kingdee.BOS.ServiceHelper;
using Kingdee.BOS.Core.Metadata;
using Kingdee.BOS.Orm.DataEntity;

namespace CZ.PAEZ.SaleManager.Util
{
    public class QueueLog
    {
        public static void Log(Context context, string bill, string op)
        {
            string date = DateTime.Now.ToString();
            var saveSrv = ServiceHelper.GetService<ISaveService>();
            var meta = MetaDataServiceHelper.Load(context, "PAEZ_Sal_QueueLog") as FormMetadata;
            var model = new DynamicObject(meta.BusinessInfo.GetDynamicObjectType());
            model["FOpDate"] = date;
            model["FQueueBill"] = bill;
            model["FQueueOp"] = op;
            model["FOperatorId_Id"] = context.UserId;
            var data = new DynamicObject[] { model };
            saveSrv.SaveAndAudit(context, meta.BusinessInfo, data);
        }
    }
}
