## Cloud 项目源码

### 一、 [物料变更相关的开发内容](https://gitee.com/joshualley/sscloud-src/tree/master/plugIns/CZ.PAEZ.AboutDB)

| 表单                 | 功能                                              | 插件                                                         | `SQL`                                                        |
| -------------------- | ------------------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 标签打印模板适配公式 | **表单插件**<br/>更新实际打标标志                 | [LabelPrintFormulaForm](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.AboutDB/Forms/LabelPrintFormulaForm.cs) | [proc_czly_UpdateMtlRealPrintLabel](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.AboutDB/Sql/更新物料的实际打标标志.sql) |
| 物料                 | **表单插件**<br/>更新实际打标标志                 | [MaterialForm](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.AboutDB/Forms/MaterialForm.cs) | [proc_czly_UpdateMtlRealPrintLabel](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.AboutDB/Sql/更新物料的实际打标标志.sql) |
| 物料数据修改审批表单 | **表单插件**<br/>1、携带物料字段<br/>2、生成`UDI` | [MtlModifyForm](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.AboutDB/Forms/MtlModifyForm.cs) |                                                              |
| 目录外产品加工申请单 | **表单插件**<br/>获取定制件编码、批号             | [OutOfCataProdApplyForm](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.AboutDB/Forms/OutOfCataProdApplyForm.cs) | [func_czly_GeneCustomizedMtlNumber](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.AboutDB/Sql/fun生成定制件物料的编码规则.sql) |
| `UDI`最大码          | **表单插件**<br/>获取`UDI`最大码                  | [UDIMaxIDForm](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.AboutDB/Forms/UDIMaxIDForm.cs) |                                                              |
| 物料数据修改审批单   | **服务插件**<br/>审核时更新物料                   | [MtlModifyAudit](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.AboutDB/Services/MtlModifyAudit.cs) | [proc_czly_ModifyMtlProperty](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.AboutDB/Sql/根据单据类型修改物料属性.sql)<br/>[proc_czly_GeneMtlModifyRecord](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.AboutDB/Sql/增加物料变更记录.sql) |

### 二、 [销售相关的开发内容](https://gitee.com/joshualley/sscloud-src/tree/master/plugIns/CZ.PAEZ.SaleManager)

#### 1.插件

| 表单            | 功能                                                         | 插件                                                         | `SQL`                                                        |
| --------------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 销售订单        | **表单插件**<br>1、获取参考价格<br>2、判断是否为低价订单<br>3、校验低价订单类型 | [SaleOrder](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.SaleManager/FormPlugIns/SaleOrder.cs) |                                                              |
| 销售订单        | **列表插件**<br>销售订单排队计算是否允许下推提货联           | [SaleOrderQueue](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.SaleManager/ListPlugIns/SaleOrderQueue.cs) | [proc_czly_SaleOrderQueue](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.SaleManager/SQLs/销售订单排队-v6.sql) |
| 销售订单        | **服务插件**<br>审核时写入排队日期                           | [销售订单审核.py](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.SaleManager/PyPlugIns/销售订单审核.py) |                                                              |
| 生产订单        | **表单插件**<br>控制非返工的生产订单中一个物料仅能对应一个批号 | [ProMoOrder](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.SaleManager/FormPlugIns/ProMoOrder.cs) |                                                              |
| 生产订单        | **列表插件**<br/>生产订单中订配包物料排队                    | [PrdMoQueue](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.SaleManager/ListPlugIns/PrdMoQueue.cs) | [proc_czly_SaleOrderQueue](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.SaleManager/SQLs/销售订单排队-v5.sql) |
| 借用/售后申请单 | **列表插件**<br/>借用、售后排队                              | [NonsaleApplyQueue](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.SaleManager/ListPlugIns/NonsaleApplyQueue.cs) | [proc_czly_SaleOrderQueue](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.SaleManager/SQLs/销售订单排队-v6.sql) |
| 工序计划单      | **表单插件**<br/>返工单中至少存在一个入库点                  | [OperationPlanning](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.SaleManager/FormPlugIns/OperationPlanning.cs) |                                                              |
| 发票池录入      | **动态表单插件**<br>批量录入发票                             | [InvoicePoolInDyForm](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.SaleManager/FormPlugIns/InvoicePoolInDyForm.cs) |                                                              |
| 发票池          | **Python列表插件**<br>发票池标记为已经使用                   | [发票池列表.py](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.SaleManager/PyPlugIns/发票池列表.py) |                                                              |
| 金税盘选择      | **动态表单插件**<br>为提货联获取发票号时提供金税盘号         | [GoldTakDiskChoice](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.SaleManager/FormPlugIns/GoldTakDiskChoice.cs) |                                                              |
| 提货联          | **表单插件**<br/>1、从发票池获取发票号<br>2、行作废          | [PickGoodsForm](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.SaleManager/FormPlugIns/PickGoodsForm.cs) |                                                              |
| 提货联          | **列表插件**<br/>从发票池获取发票号                          | [PickGoodsList](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.SaleManager/ListPlugIns/PickGoodsList.cs) |                                                              |
| 发货汇总单      | **表单插件**<br>保存时计算发票数量                           | [SaleoutSum](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.SaleManager/FormPlugIns/SaleoutSum.cs) |                                                              |
| 发货汇总单      | **服务插件**<br/>1、计算分单号<br>2、计算发票数量、发货通知单号 | [SaleoutSumSave](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.SaleManager/ServicePlugIns/SaleoutSumSave.cs) |                                                              |
| 普通、专用发票  | **列表插件**<br>调整断号的发票号                             | [InvoiceList](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.SaleManager/ListPlugIns/InvoiceList.cs) |                                                              |
| 发货通知单      | **服务插件**<br/>将携带的编号写入到单据编号上                | [DeliverNotifySave](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.SaleManager/ServicePlugIns/DeliverNotifySave.cs) |                                                              |
| 检验单          | **表单插件**<br>判定是否合格                                 | [检验单.py](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.SaleManager/PyPlugIns/检验单.py) |                                                              |
| 检验单          | **列表插件**<br>优科：将列表中所选单据标为不良品             | [检验单列表.py](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.SaleManager/PyPlugIns/检验单列表.py) |                                                              |
| 检验单          | **服务插件**<br/>审核后将检验单单号反写至关联的发货通知单    | [InspectBillAudit](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.SaleManager/ServicePlugIns/InspectBillAudit.cs) |                                                              |
| 生产用料清单    | **服务插件**<br/>审核时将用料清单中编码以_S结尾的物料的批号刷为生产订单对应行上的批号 | [PPBomAudit](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.SaleManager/ServicePlugIns/PPBomAudit.cs) |                                                              |
| 费用项目        | **表单插件**<br/>费用项目序号类别绑定                        | [费用项目获取类别序号.py](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.SaleManager/PyPlugIns/费用项目获取类别序号.py) |                                                              |
| 差旅费报销单    | **表单插件**<br/>计算出差时间                                | [ER_ExpReimbursement_Travel.py](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.SaleManager/PyPlugIns/ER_ExpReimbursement_Travel.py) |                                                              |
| 费用报销单      | **表单插件**<br>携带部门负责人                               | [ExpReimbursementForm](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.SaleManager/FormPlugIns/ExpReimbursementForm.cs) |                                                              |
| 付款申请单      | **表单插件**<br>携带部门负责人                               | [付款申请单由部门携带部门负责人用户.py](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.SaleManager/PyPlugIns/付款申请单由部门携带部门负责人用户.py) |                                                              |
| 强制库存保留单  | **表单构建插件**<br/>显示表体过滤行                          | [强制库存保留构建.py](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.SaleManager/PyPlugIns/强制库存保留构建.py) |                                                              |
| 开票通知单      | **表单插件**<br/>携带供应商邮箱                              | [开票通知单携带供应商邮箱.py](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.SaleManager/PyPlugIns/开票通知单携带供应商邮箱.py) |                                                              |

#### 2. 其他

| 功能                   | `SQL`                                                        | 描述                                         |
| ---------------------- | ------------------------------------------------------------ | -------------------------------------------- |
| 发货数量核对报表       | [proc_czly_CheckSaleDelvQty](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.SaleManager/SQLs/发货数量核对-v2.sql) | 核对销售发货线上各单据中的数量               |
| 获取物料历史记录       | [fun_czly_GetMtlHistoryRecord](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.SaleManager/SQLs/fun获取物料历史记录.sql) | 获取物料历史记录                             |
| 获取物料库存及发货占用 | [fun_czly_GetMtlStkOcupy](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.SaleManager/SQLs/fun获取物料库存及占用-v2.sql) | 排队时获取物料库存及发货占用                 |
| 物料库存占用查询报表   | [proc_czly_MtlStkOcupyQuery](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.SaleManager/SQLs/物料库存占用查询.sql) |                                              |
| 获取物料生产企业批号   | [fun_czly_GetMtlSupplierInfoCp](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.SaleManager/SQLs/fun获取物料生产企业批号CP.sql) | 获取物料生产企业、供应商批号、生产许可证号   |
| 排队结果查询报表       | [proc_czly_QueueResult](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.SaleManager/SQLs/排队查询v2.sql) | 排队结果查询                                 |
| 提货联下推错误报表     | [proc_czly_CheckLadingError](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.SaleManager/SQLs/提货联下推错误报表.sql) | 检查提货联是否错误下推                       |
| 费用报表基础函数       | [fun_czly_GetFeeData](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.SaleManager/SQLs/fun费用报表base.sql) | 费用报表基础函数：根据年度、科目获取费用数据 |
| 销售费用报表           | [proc proc_czly_SaleFeeRpt](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.SaleManager/SQLs/销售费用报表.sql) |                                              |
| 管理费用报表           | [proc_czly_ManagerFeeRpt](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.SaleManager/SQLs/管理费用报表.sql) |                                              |
| 研发费用报表           | [proc_czly_ResearchFeeRpt](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.SaleManager/SQLs/研发费用报表.sql) |                                              |
| 研发费用明细表         | [proc_czly_ResearchFeeDetailRpt](https://gitee.com/joshualley/sscloud-src/blob/master/plugIns/CZ.PAEZ.SaleManager/SQLs/研发费用明细表.sql) |                                              |

